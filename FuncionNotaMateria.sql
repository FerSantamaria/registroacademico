USE colegio
GO

CREATE FUNCTION NotaMateria(@idMatricula int, @idMateria int, @idPeriodo int) RETURNS float
AS
BEGIN
	DECLARE @nota float = 0;
	DECLARE @notaFinal float = 0;
	
	DECLARE cursorNotas CURSOR FOR
	SELECT ((CAST(detalleTarea.ponderacion AS float) / CAST(100 AS float)) * notaTarea.nota) AS Nota FROM notaTarea  
	INNER JOIN detalleTarea ON notaTarea.idTarea = detalleTarea.idTarea
	INNER JOIN Materia ON Materia.idMateria = detalleTarea.idMateria
	INNER JOIN Matricula ON notaTarea.idMatricula = Matricula.idMatricula
	WHERE Matricula.idMatricula=@idMatricula AND Materia.idMateria=@idMateria AND detalleTarea.idPeriodo=@idPeriodo

	OPEN cursorNotas
	FETCH NEXT FROM cursorNotas INTO @nota
	WHILE @@fetch_status = 0
	BEGIN
		SET @notaFinal += @nota
		FETCH NEXT FROM cursorNotas INTO @nota
	END
	CLOSE cursorNotas
	DEALLOCATE cursorNotas
	
	RETURN @notaFinal;
END;