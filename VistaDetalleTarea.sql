-- Procedimiento para ver informacion de la tarea
create view infoTarea as
select dtt.idTarea as id, per.Nombre + ' ' + cast(year(getdate()) as varchar(4)) as periodo, mat.Nombre as materia, gr.Nombre as grado, 
sec.Nombre as seccion, dtt.tarea as titulo, dtt.descripcion as descripcion, gr.idGrado, sec.idSeccion
from detalleTarea dtt left join Materia mat on dtt.idMateria = mat.idMateria
	left join Periodo per on dtt.idPeriodo = per.idPeriodo
	left join Grado gr on gr.idGrado = dtt.idGrado
	left join Seccion sec on sec.idSeccion = dtt.idSeccion
go

-- Procedimiento para ingresar notas
create proc ingresoNotas
	@idTarea int,
	@idMatricula int,
	@nota float
AS
	IF NOT EXISTS (select * from notaTarea where idTarea = @idTarea and idMatricula = @idMatricula)
		insert into notaTarea values (@idTarea, @idMatricula, @nota);
	ELSE
		update notaTarea set nota = @nota where idTarea = @idTarea and idMatricula = @idMatricula;
GO

USE [colegio]
GO