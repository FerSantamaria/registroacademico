﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditarGrado : System.Web.UI.Page
{
    Seccion Seccion = new Seccion();
    Grado Grado = new Grado();
    Anio Anio = new Anio();

    private void CargarSeccionesGrado()
    {
        cblSecciones.ClearSelection();

        foreach (ListItem item in cblSecciones.Items)
        {
            foreach (var sec in Grado.Secciones)
            {
                if (item.Value == sec.Value)
                {
                    item.Selected = true;
                    break;
                }
                else
                {
                    item.Selected = false;
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            ddlAnio.DataSource = Anio.Anios();
            ddlAnio.DataBind();
            //ddlAnio.SelectedIndex = 0;

            cblSecciones.DataSource = Seccion.Secciones();
            cblSecciones.DataTextField = "Text";
            cblSecciones.DataValueField = "Value";
            cblSecciones.DataBind();

            string id = Request.Params["Id"];

            try
            {
                Grado.Id = Convert.ToInt32(id);

                if (id != null && Grado.Datos(Convert.ToInt32(ddlAnio.SelectedValue)))
                {
                    txtNombre.Text = Grado.Nombre;
                    CargarSeccionesGrado();
                    Session["editGrado"] = Grado;
                }
                else
                {
                    Session["editGrado"] = null;
                    lblMensaje.Text = "Grado no encontrado. ";
                    panelSuccess.CssClass = "alert alert-danger";
                    panelSuccess.Visible = true;
                }
            }
            catch (FormatException)
            {
                Session["editGrado"] = null;
                lblMensaje.Text = "Grado no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["editGrado"] != null)
        {
            Grado = (Grado)Session["editGrado"];

            Grado.Nombre = txtNombre.Text;
            Grado.Secciones = cblSecciones.Items.Cast<ListItem>()
                            .Where(li => li.Selected)
                            .ToList();

            if (Grado.Actualizar(Convert.ToInt32(ddlAnio.SelectedValue)) > 1)
            {
                lblMensaje.Text = "Actualización exitosa. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void ddlAnio_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["editGrado"] != null)
        {
            Grado = (Grado)Session["editGrado"];

            Grado.SeccionesGrado(Convert.ToInt32(ddlAnio.SelectedValue));
            CargarSeccionesGrado();
        }
    }
}