﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class DashboardProfesor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 3)
            {
                Response.Redirect("~/Reportes.aspx");
            }
        }
        /* --- ************************ --- */

        Grado Grado = new Grado();
        DataTable grados = Grado.ListaGrados();
        int idHeading = 0;

        foreach(DataRow grado in grados.Rows)
        {
            HtmlGenericControl panel = new HtmlGenericControl("div");
            panel.Attributes["class"] = "panel panel-default";

            //Panel Header
            HtmlGenericControl panelHeading = new HtmlGenericControl("div");
            panelHeading.Attributes["class"] = "panel-heading";
            panelHeading.Attributes["role"] = "tab";
            panelHeading.Attributes["id"] = "heading" + idHeading.ToString();

            HtmlGenericControl h4Tag = new HtmlGenericControl("h4");
            h4Tag.Attributes["class"] = "panel-title";

            HtmlGenericControl aTitleTag = new HtmlGenericControl("a");
            aTitleTag.Attributes["role"] = "button";
            aTitleTag.Attributes["data-toggle"] = "collapse";
            aTitleTag.Attributes["data-parent"] = "#accordion";
            aTitleTag.Attributes["href"] = "#grado" + idHeading.ToString();
            aTitleTag.Attributes["aria-expanded"] = "false";
            aTitleTag.Attributes["aria-controls"] = "grado" + idHeading.ToString();
            aTitleTag.InnerText = grado[2].ToString() + " - Sección " + grado[3].ToString();

            HtmlGenericControl iconTag = new HtmlGenericControl("i");
            iconTag.Attributes["class"] = "fa fa-caret-down pull-right";

            //Panel Body
            HtmlGenericControl panelBody = new HtmlGenericControl("div");
            panelBody.Attributes["id"] = "grado" + idHeading.ToString();
            panelBody.Attributes["class"] = "panel-collapse collapse";
            panelBody.Attributes["role"] = "tabpanel";
            panelBody.Attributes["aria-expanded"] = "false";
            panelBody.Attributes["aria-labelledby"] = "heading" + idHeading.ToString();

            HtmlGenericControl body = new HtmlGenericControl("div");
            body.Attributes["class"] = "panel-body";

            HtmlGenericControl center = new HtmlGenericControl("center");

            //Listado de Alumnos
            HtmlGenericControl link1 = new HtmlGenericControl("a");
            link1.Attributes["href"] = "ListaAlumnosGrado.aspx?grado=" + grado[0] + "&seccion=" + grado[1];
            link1.Attributes["class"] = "btn btn-info";            

            HtmlGenericControl iconLink1 = new HtmlGenericControl("i");
            iconLink1.Attributes["class"] = "fa fa-users fa-3x";            

            HtmlGenericControl spanText1 = new HtmlGenericControl("span");
            spanText1.InnerHtml = "Listado de Alumnos";

            //Listado de Asignaturas
            HtmlGenericControl link2 = new HtmlGenericControl("a");
            link2.Attributes["href"] = "ListaAsignaturasProfesor.aspx?grado=" + grado[0] + "&seccion=" + grado[1];
            link2.Attributes["class"] = "btn btn-info";

            HtmlGenericControl iconLink2 = new HtmlGenericControl("i");
            iconLink2.Attributes["class"] = "fa fa-book fa-3x";

            HtmlGenericControl spanText2 = new HtmlGenericControl("span");
            spanText2.InnerHtml = "Listado de Asignaturas";

            //Portafolio de Actividades
            HtmlGenericControl link3 = new HtmlGenericControl("a");
            link3.Attributes["href"] = "ListaActividadesPortafolio.aspx?grado=" + grado[0] + "&seccion=" + grado[1];
            link3.Attributes["class"] = "btn btn-info";

            HtmlGenericControl iconLink3 = new HtmlGenericControl("i");
            iconLink3.Attributes["class"] = "fa fa-list fa-3x";

            HtmlGenericControl spanText3 = new HtmlGenericControl("span");
            spanText3.InnerHtml = "Portafolio de Actividades";

            //Registro de Calificaciones
            HtmlGenericControl link4 = new HtmlGenericControl("a");
            link4.Attributes["href"] = "ListaActividadesCalificaciones.aspx?grado=" + grado[0] + "&seccion=" + grado[1];
            link4.Attributes["class"] = "btn btn-info";

            HtmlGenericControl iconLink4 = new HtmlGenericControl("i");
            iconLink4.Attributes["class"] = "fa fa-check-square-o fa-3x";

            HtmlGenericControl spanText4 = new HtmlGenericControl("span");
            spanText4.InnerHtml = "Registro de Calificaciones";

            acordionGrados.Controls.Add(panel);
            //Add panel header
            panel.Controls.Add(panelHeading);
            panelHeading.Controls.Add(h4Tag);
            h4Tag.Controls.Add(aTitleTag);
            aTitleTag.Controls.Add(iconTag);

            Label[] space = new Label[3];
            for (int cont = 0; cont <= 2; cont++)
            {
                space[cont] = new Label();
                space[cont].Text = "&nbsp;";
            }

            //Add panel body
            panel.Controls.Add(panelBody);
            panelBody.Controls.Add(body);
            body.Controls.Add(center);
            center.Controls.Add(link1);
            center.Controls.Add(space[0]);
            center.Controls.Add(link2);
            center.Controls.Add(space[1]);
            center.Controls.Add(link3);
            center.Controls.Add(space[2]);
            center.Controls.Add(link4);

            link1.Controls.Add(new LiteralControl("<br/>"));
            link1.Controls.Add(iconLink1);
            link1.Controls.Add(new LiteralControl("<br/>"));
            link1.Controls.Add(new LiteralControl("<br/>"));
            link1.Controls.Add(spanText1);

            link2.Controls.Add(new LiteralControl("<br/>"));
            link2.Controls.Add(iconLink2);
            link2.Controls.Add(new LiteralControl("<br/>"));
            link2.Controls.Add(new LiteralControl("<br/>"));
            link2.Controls.Add(spanText2);

            link3.Controls.Add(new LiteralControl("<br/>"));
            link3.Controls.Add(iconLink3);
            link3.Controls.Add(new LiteralControl("<br/>"));
            link3.Controls.Add(new LiteralControl("<br/>"));
            link3.Controls.Add(spanText3);

            link4.Controls.Add(new LiteralControl("<br/>"));
            link4.Controls.Add(iconLink4);
            link4.Controls.Add(new LiteralControl("<br/>"));
            link4.Controls.Add(new LiteralControl("<br/>"));
            link4.Controls.Add(spanText4);


            idHeading = idHeading + 1;
        }
    }
}