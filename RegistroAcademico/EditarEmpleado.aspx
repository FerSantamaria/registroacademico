﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="EditarEmpleado.aspx.cs" Inherits="EditarEmpleado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Editar empleados</h2>
    <h3>Información empleado</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaEmpleados.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />

    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nombres" ControlToValidate="txtNombres" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Primer apellido" ControlToValidate="txtPrimerApellido" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Dirección" ControlToValidate="txtDireccion" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Teléfono" ControlToValidate="txtTelefono" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Cargo" ControlToValidate="ddcargo" Display="None"></asp:RequiredFieldValidator>

    <div class="form-group col-sm-12">
        <label for="txtNombres">Nombres:</label>
        <asp:TextBox ID="txtNombres" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtPrimerApellido">Primer Apellido:</label>
        <asp:TextBox ID="txtPrimerApellido" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtSegundoApellido">Segundo Apellido:</label>
        <asp:TextBox ID="txtSegundoApellido" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-12">
        <label for="txtDireccion">Dirección:</label>
        <asp:TextBox ID="txtDireccion" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtTelefono">Teléfono:</label>
        <asp:TextBox ID="txtTelefono" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="ddcargo">Cargo:</label>
        <asp:DropDownList ID="ddcargo" CssClass="form-control" runat="server">
            <asp:ListItem Value="1">Profesor</asp:ListItem>
            <asp:ListItem Value="2">Dirección</asp:ListItem>
            <asp:ListItem Value ="3">Secretaría</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="col-md-12">
        <asp:Button ID="btnGuardar" CssClass="btn btn-info pull-right" runat="server" Text="Actualizar empleado" OnClick="btnGuardar_Click" />
    </div>

    <div class="divide-20"></div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

