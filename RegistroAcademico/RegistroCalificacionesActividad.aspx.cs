﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroCalificacionesActividad : System.Web.UI.Page
{
    int idTarea;
    string grado;
    string seccion;

    Tareas Tareas = new Tareas();
    Alumnos Alumnos = new Alumnos();
    
    //DataTables
    DataTable datos = new DataTable();
    DataTable dt = new DataTable();
    DataTable dtNotas = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.idTarea = Convert.ToInt32(Request.QueryString["id"]);
        dt = Tareas.DetalleTarea(idTarea);

        lblPeriodo.Text = dt.Rows[0][1].ToString();
        lblMateria.Text = dt.Rows[0][2].ToString();
        lblGrado.Text = dt.Rows[0][3].ToString();
        lblSeccion.Text = dt.Rows[0][4].ToString();
        lblTitulo.Text = dt.Rows[0][5].ToString();
        lblDescripcion.Text = dt.Rows[0][6].ToString();
        this.grado = dt.Rows[0][7].ToString();
        this.seccion = dt.Rows[0][8].ToString();

        datos = Alumnos.ListadoAlumnoGrado(this.grado, this.seccion).Tables[0];

        if (!IsPostBack)
        {
            dtNotas = Tareas.notasAlumnos(idTarea);
            if(dtNotas.Rows.Count > 0)
            {
                int cont = 0;
                gdvAlumnos.DataSource = dtNotas;
                gdvAlumnos.DataBind();
                foreach (GridViewRow gvdRow in gdvAlumnos.Rows)
                {                    
                    TextBox txtNota = (TextBox)gvdRow.FindControl("txtNota");                    
                    txtNota.Text = String.Format("{0:0.##}", dtNotas.Rows[cont][2]); //((float)(Math.Round((double)grade, 2))).ToString();
                    cont++;
                }
            }
            else
            {
                gdvAlumnos.DataSource = datos;
                gdvAlumnos.DataBind();
            }            
        }
    }

    protected void btnRegistrar_Click(object sender, EventArgs e)
    {
        int tarea = Convert.ToInt32(Request.QueryString["id"]);
        int matricula;
        float nota;

        int rsQuery = 0;

        int cont = 0;
        
        foreach (GridViewRow gvdRow in gdvAlumnos.Rows)
        {
            matricula = Convert.ToInt32(datos.Rows[cont][1]);
            TextBox txtNota = (TextBox)gvdRow.FindControl("txtNota");
            nota = float.Parse(txtNota.Text);
            rsQuery = Tareas.ingresarNotas(tarea, matricula, nota);
            if(rsQuery > 0)
            {
                cont++;
                continue;
            }
            else
            {
                break;
            }            
        }

        if(rsQuery > 0)
        {
            lblMensaje.Text = "Registro de notas exitoso";
            HyperLink1.NavigateUrl = "ListaActividadesCalificaciones.aspx?grado=" + this.grado + "&seccion="+this.seccion;
            panelSuccess.Visible = true;
        }
        else
        {
            panelSuccess.CssClass = "alert alert-danger";
            lblMensaje.Text = "Ha ocurrido un error al guardar.";
            panelSuccess.Visible = true;
        }
    }
}