﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DashboardReportes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Grado Grado = new Grado();
        Periodo Periodo = new Periodo();

        if (!Page.IsPostBack)
        {
            // Dropdown de grados
            ddlGrado.DataSource = Grado.Grados();
            ddlGrado.DataTextField = "Text";
            ddlGrado.DataValueField = "Value";
            ddlGrado.DataBind();

            // Dropdown de grados final
            ddlGradoFinal.DataSource = Grado.Grados();
            ddlGradoFinal.DataTextField = "Text";
            ddlGradoFinal.DataValueField = "Value";
            ddlGradoFinal.DataBind();

            // Dropdown de periodos
            ddlPeriodo.DataSource = Periodo.Periodos();
            ddlPeriodo.DataTextField = "Text";
            ddlPeriodo.DataValueField = "Value";
            ddlPeriodo.DataBind();
        }
    }

    protected void btnReportePeriodo_Click(object sender, EventArgs e)
    {
        Response.Redirect("ReportePeriodoGrado.aspx?periodo=" + ddlPeriodo.SelectedValue + "&grado=" + ddlGrado.SelectedValue);
    }

    protected void btnReporteFinal_Click(object sender, EventArgs e)
    {
        Response.Redirect("ReporteFinalGrado.aspx?grado=" + ddlGradoFinal.SelectedValue);
    }
}