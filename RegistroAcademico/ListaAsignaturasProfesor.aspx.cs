﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaAsignaturasProfesor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 3)
            {
                Response.Redirect("~/Reportes.aspx");
            }
        }
        /* --- ************************ --- */

        //<-- DATOS PRUEBA DISEÑO -->
        DataTable datos = new DataTable();
        datos.Columns.Add("idMateria");
        datos.Columns.Add("Nombre");

        DataRow dr = datos.NewRow();
        dr["idMateria"] = "1";
        dr["Nombre"] = "Matemática";

        datos.Rows.Add(dr);

        DataRow dr1 = datos.NewRow();
        dr1["idMateria"] = "2";
        dr1["Nombre"] = "Lenguaje";

        datos.Rows.Add(dr1);

        gdvAsignaturas.DataSource = datos;
        gdvAsignaturas.DataBind();
        //<!-- DATOS PRUEBA DISEÑO -->
    }
}