﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaCalificacionesActividad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //<-- DATOS PRUEBA DISEÑO -->
        DataTable datos = new DataTable();
        datos.Columns.Add("NIE");
        datos.Columns.Add("Nombre");
        datos.Columns.Add("Nota");

        DataRow dr = datos.NewRow();
        dr["NIE"] = "FS123456";
        dr["Nombre"] = "José Fernando Flores Santamaría";
        dr["Nota"] = 5.5;
        datos.Rows.Add(dr);

        DataRow dr1 = datos.NewRow();
        dr1["NIE"] = "MM123456";
        dr1["Nombre"] = "Gustavo Alejandro Müller Mancía";
        dr1["Nota"] = 9.75;
        datos.Rows.Add(dr1);

        DataRow dr2 = datos.NewRow();
        dr2["NIE"] = "MR123456";
        dr2["Nombre"] = "José Augusto Meza Rivera";
        dr2["Nota"] = 10.00;
        datos.Rows.Add(dr2);

        gdvAlumnos.DataSource = datos;
        gdvAlumnos.DataBind();
        //<!-- DATOS PRUEBA DISEÑO -->
    }
}