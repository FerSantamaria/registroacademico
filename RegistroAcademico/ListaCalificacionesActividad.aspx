﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaCalificacionesActividad.aspx.cs" Inherits="ListaCalificacionesActividad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Calificaciones</h2>
    
    <h3 class="sub-header">Consulta de calificaciones</h3>

    <div class="form-group col-md-12">
        <h5><strong>Período:</strong> <asp:Label ID="lblPeriodo" runat="server" Text="01 2018"></asp:Label></h5>
        <h5><strong>Matería:</strong> <asp:Label ID="lblMateria" runat="server" Text="Matemática"></asp:Label></h5>
        <h5><strong>Grado:</strong> <asp:Label ID="lblGrado" runat="server" Text="Kinder 4"></asp:Label></h5>
        <h5><strong>Sección:</strong> <asp:Label ID="lblSeccion" runat="server" Text="A"></asp:Label></h5>
    </div>

    <div class="form-group col-md-12">
        <h5><strong>Título:</strong> <asp:Label ID="Label1" runat="server" Text="Tarea Ex-Aula"></asp:Label></h5>
        <h5><strong>Descripción:</strong> <asp:Label ID="Label2" runat="server" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris condimentum cursus neque, ac vulputate ante mattis non."></asp:Label></h5>
    </div>

    <div class="col-md-8">
        <div class="table-responsive" id="tblListaUsuarios">
            <asp:GridView ID="gdvAlumnos" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="NIE"  ItemStyle-Width="10%" HeaderText="NIE"/>
                    <asp:BoundField DataField="Nombre"  ItemStyle-Width="50%" HeaderText="Nombre"/>
                    <asp:BoundField DataField="Nota" ItemStyle-Width="10%" HeaderText="Nota"/>
                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

