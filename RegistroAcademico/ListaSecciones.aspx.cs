﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaMaterias : System.Web.UI.Page
{
    Seccion Seccion = new Seccion();

    protected void dgvBind()
    {
        DataSet ds = new DataSet();
        ds = Seccion.Listado();

        gdvSecciones.DataSource = ds;
        gdvSecciones.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        dgvBind();
    }
}