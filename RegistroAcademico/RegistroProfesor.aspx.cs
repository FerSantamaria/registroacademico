﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroProfesor : System.Web.UI.Page
{
    Profesor Profesor = new Profesor();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        ddEmpleado.DataSource = Profesor.ProfsNoReg();
        ddEmpleado.DataBind();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Profesor.Id = ddEmpleado.SelectedValue;
        Profesor.Titulo = txtTitulo.Text;
        Profesor.Especialidad = txtEspecialidad.Text;

        if (Profesor.Crear() > 0)
        {
            lblMensaje.Text = "Registro exitoso. ";
            panelSuccess.Visible = true;
        }
        else
        {
            panelSuccess.CssClass = "alert alert-danger";
            lblMensaje.Text = "Ha ocurrido un error al guardar.";
            panelSuccess.Visible = true;
        }
    }
}