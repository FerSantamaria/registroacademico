﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="RegistroAsignaturasProfesor.aspx.cs" Inherits="RegistroMateriasProfesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Profesores</h2>
    <h3>Registro de asignaturas</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaProfesor.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />
    
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Grado" ControlToValidate="ddlGrado" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Sección" ControlToValidate="ddlSeccion" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Año Lectivo" ControlToValidate="ddlAnio" Display="None"></asp:RequiredFieldValidator>
    <asp:CustomValidator runat="server" ID="CustomFieldValidator1" ClientValidationFunction="ValidateModuleList" ErrorMessage="Asignaturas (al menos una)" Display="None" ></asp:CustomValidator>


    <div class="divide-10"></div>

    <div class="form-group col-sm-12">
        <h4><strong>Profesor:</strong> <asp:Label ID="lblProfesor" runat="server" Text=""></asp:Label></h4>
    </div>

    <div class="col-md-5">
        <h4 class="sub-header">Asignaturas</h4>
        <div class="form-group col-sm-12">
            <label for="ddlGrado">Año Lectivo:</label>
            <asp:DropDownList ID="ddlAnio" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAnio_SelectedIndexChanged">
                <%-- Años lectivos --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-sm-6">
            <label for="ddlGrado">Grado:</label>
            <asp:DropDownList ID="ddlGrado" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGrado_SelectedIndexChanged">
                <%-- Grados --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-sm-6">
            <label for="ddlSeccion">Sección:</label>
            <asp:DropDownList ID="ddlSeccion" CssClass="form-control" runat="server">
                <%-- Secciones --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-sm-12">
            <asp:GridView ID="gdvAsignaturasGrado" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="idMateria" HeaderText="ID" />
                    <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                    <asp:TemplateField ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                        <HeaderTemplate>
                            Todo<br /> <input type="checkbox" runat="server" title="Seleccionar Todo" class="chkTodo"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkRow" CssClass="chkRow" ToolTip="Seleccionar" runat="server" />
                            <%--<input type="checkbox" runat="server" class="chkRow" title="Seleccionar" id="chkRow"/>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>Sin Registros.</EmptyDataTemplate>
            </asp:GridView>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-sm-12">
            <asp:Button ID="btnGuardar" CssClass="btn btn-success" runat="server" Text="Guardar Cambios" OnClick="btnGuardar_Click" />
        </div>
    </div>

    <div class="col-md-7">
        <h4 class="sub-header">Histórico de Asignaturas</h4>
        <div class="table-responsive" id="tblListaMatriculas">
            <asp:GridView ID="gdvAsignaturasProfesor" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true" >
                <Columns>
                    <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                    <asp:BoundField DataField="Grado"  HeaderText="Grado"/>        
                    <asp:BoundField DataField="Seccion"  HeaderText="Sección"/>
                    <asp:BoundField DataField="anioLectivo"  HeaderText="Año"/>
                </Columns>
                <EmptyDataTemplate>Sin Registros.</EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
    <div class="divide-10"></div>

</asp:Content>

<asp:Content ID="js" ContentPlaceHolderID="customJavascript" runat="server">
    <script type="text/javascript">
        $(document).on('ready', function () {

            $('.chkTodo').change(function() {
                if (this.checked) {
                   $('.chkRow').prop( "checked", true );
                } else {
                   $('.chkRow').prop( "checked", false );
                }       
            });
        });

        function ValidateModuleList(source, args)
        {
            var chkListModules= document.getElementById ('<%= gdvAsignaturasGrado.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i=0;i<chkListinputs.length;i++)
            {
                if (chkListinputs[i].checked)
                {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>
