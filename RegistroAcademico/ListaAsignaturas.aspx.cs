﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaMaterias : System.Web.UI.Page
{
    Materia Materia = new Materia();

    protected void dgvBind()
    {
        DataSet ds = new DataSet();

        if (!IsPostBack || (IsPostBack && txtBusqueda.Text == ""))
            ds = Materia.Listado();
        else
            ds = Materia.Listado(txtBusqueda.Text);

        gdvAsignaturas.DataSource = ds;
        gdvAsignaturas.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        dgvBind();
    }
}