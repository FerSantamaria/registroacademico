﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroAlumno : System.Web.UI.Page
{
    Alumnos alum = new Alumnos();
    Departamentos dep = new Departamentos();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack){
            ddlDepartamento.DataSource = dep.departamentos();
            ddlDepartamento.DataValueField = "ID";
            ddlDepartamento.DataTextField = "DepName";
            ddlDepartamento.DataBind();
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        bool[] checks = new bool[3];
        int i = 0;
        int ret;
        foreach (ListItem listItem in CheckBoxList1.Items)
        {
            if (listItem.Selected)
            {
                checks[i] = true;
            }
            else
            {
                checks[i] = false;
            }
            i++;
        }

        ret = alum.registrarAlumno(txtNIE.Text, txtNombres.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, ddlDepartamento.SelectedValue, ddlMunicipio.SelectedValue, txtPartida.Text, Convert.ToInt32(txtFolio.Text), Convert.ToInt32(ddlSexo.SelectedValue), txtDireccion.Text, txtVive.Text, txtResponsable.Text, txtDUI.Text, txtProfResp.Text, Convert.ToInt32(txtMiembros.Text), Convert.ToInt32(txtNoLeen.Text), Convert.ToInt32(txtEstCivil.SelectedValue), txtTelefono.Text, (calNacimiento.Text), (calIngreso.Text), txtContEmergencia.Text, txtParentesco.Text, txtReligion.Text, Convert.ToInt32(checks[0]), Convert.ToInt32(checks[1]), Convert.ToInt32(checks[2]), txtAlergias.Text);

        if (ret > 0)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('¡Alumno agregado exitósamente!')", true);

            panelSuccess.CssClass = "alert alert-success";
            lblMensaje.Text = "Registro exitoso. ";
            panelSuccess.Visible = true;
        }
    }

    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idDep = Convert.ToInt32(ddlDepartamento.SelectedValue);
        ddlMunicipio.DataSource = dep.municipios(idDep);
        ddlMunicipio.DataValueField = "ID";
        ddlMunicipio.DataTextField = "MunName";
        ddlMunicipio.DataBind();
    }
}