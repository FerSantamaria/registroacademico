﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="EliminarAlumno.aspx.cs" Inherits="EliminarAlumno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="sub-header">Eliminar Alumno</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaAlumnos.aspx">lista</a>
    </asp:Panel>

    <div class="form-group col-md-12">
        <h5><strong>Código:</strong> <asp:Label ID="lblCodigo" runat="server" Text=""></asp:Label></h5>
        <h5><strong>Nombre:</strong> <asp:Label ID="lblNombre" runat="server" Text=""></asp:Label></h5>
        <h5><strong>NIE:</strong> <asp:Label ID="lblNie" runat="server" Text=""></asp:Label></h5>
        <h5><strong>Fecha registrado:</strong> <asp:Label ID="lblFecha" runat="server" Text=""></asp:Label></h5>
    </div>

    <div class="form-group col-md-12 text-right">
        <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger" OnClick="btnEliminar_Click" />
        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-info" NavigateUrl="~/listaAlumnos.aspx">Cancelar</asp:HyperLink>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

