﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="DashboardProfesor.aspx.cs" Inherits="DashboardProfesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Registro de calificaciones</h2>
    <h3 class="sub-header">Grados</h3>

    <div class="divide-10"></div>

    <div class="panel-group" id="acordionGrados" role="tablist" aria-multiselectable="true" runat="server">
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
    <script type="text/javascript">
       
        $(document).on('ready', function () {
             //$('#acordionGrados .in').collapse('hide');
            //$('.collapse').on('shown.bs.collapse', function(){
            //    $(this).parent().find(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-up");
            //}).on('hidden.bs.collapse', function(){
            //    $(this).parent().find(".fa-caret-up").removeClass("fa-caret-up").addClass("fa-caret-down");
            //});
        });
    </script>
    
</asp:Content>

