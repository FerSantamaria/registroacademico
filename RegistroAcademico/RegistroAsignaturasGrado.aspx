﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="RegistroAsignaturasGrado.aspx.cs" Inherits="RegistroAsignaturasGrado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Grados</h2>
    <h3>Registro de currícula</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaGrados.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />
    
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Año Lectivo" ControlToValidate="ddlAnio" Display="None"></asp:RequiredFieldValidator>
    <asp:CustomValidator runat="server" ID="CustomFieldValidator1" ClientValidationFunction="ValidateModuleList" ErrorMessage="Asignaturas (al menos una)" Display="None" ></asp:CustomValidator>

    <div class="divide-10"></div>

    <div class="form-group col-sm-12">
        <h4><strong>Grado:</strong> <asp:Label ID="lblGrado" runat="server" Text=""></asp:Label></h4>
    </div>

    <div class="col-md-5">
        <h4 class="sub-header">Asignaturas</h4>
        <div class="form-group col-sm-12">
            <label for="ddlAnio">Año Lectivo:</label>
            <asp:DropDownList ID="ddlAnio" CssClass="form-control" runat="server">
                <%-- Años lectivos --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-sm-12">

            <asp:GridView ID="gdvAsignaturasGrado" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" ShowHeaderWhenEmpty="true" GridLines="None">
                <Columns>
                    <asp:BoundField DataField="idMateria"  HeaderText="ID"/>
                    <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                    <asp:TemplateField ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                        <HeaderTemplate>
                            Todo<br /> <input type="checkbox" runat="server" title="Seleccionar Todo" class="chkTodo"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkRow" CssClass="chkRow" ToolTip="Seleccionar" runat="server" />
                            <%--<input type="checkbox" runat="server" class="chkRow" title="Seleccionar" id="chkRow"/>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>Sin Registros</EmptyDataTemplate>
            </asp:GridView>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-sm-12">
            <asp:Button ID="btnGuardar" CssClass="btn btn-success" runat="server" Text="Guardar Cambios" OnClick="btnGuardar_Click"/>
        </div>
    </div>

    <div class="col-md-7">
        <h4 class="sub-header">Histórico de Asignaturas</h4>
        <div class="table-responsive" id="tblListaMatriculas">
            <asp:GridView ID="gdvAsignaturasHistorico" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" ShowHeaderWhenEmpty="true" GridLines="None" >
                <Columns>
                    <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                    <asp:BoundField DataField="anioLectivo"  HeaderText="Año"/>
                </Columns>
                <EmptyDataTemplate>Sin Registros</EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
    <div class="divide-10"></div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
    <script type="text/javascript">
        $(document).on('ready', function () {

            $('.chkTodo').change(function() {
                if (this.checked) {
                   $('.chkRow').prop( "checked", true );
                } else {
                   $('.chkRow').prop( "checked", false );
                }       
            });
        });

        function ValidateModuleList(source, args)
        {
            var chkListModules= document.getElementById ('<%= gdvAsignaturasGrado.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            //var chkListinputs = chkListModules.getElementById("chkRow");
            alert("Aqui");
            for (var i=0;i<chkListinputs.length;i++)
            {
                if (chkListinputs[i].checked)
                {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>