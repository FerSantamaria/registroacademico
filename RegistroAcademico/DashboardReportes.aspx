﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="DashboardReportes.aspx.cs" Inherits="DashboardReportes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <h2 class="header">Reportes</h2>
    <h3 class="sub-header">Reportes de calificaciones por período y grado</h3>

    <div class="col-md-12">
        <div class="form-group col-md-2">
            <label for="ddlAnio">Período:</label>
            <asp:DropDownList ID="ddlPeriodo" CssClass="form-control" runat="server">
                <%-- Periodos --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-md-3">
            <label for="ddlAnio">Grado:</label>
            <asp:DropDownList ID="ddlGrado" CssClass="form-control" runat="server">
                <%-- Grados --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-md-7">
            <div class="divide-25"></div>
            <asp:Button ID="btnReportePeriodo" CssClass="btn btn-primary" runat="server" Text="Generar Reporte" OnClick="btnReportePeriodo_Click" />
        </div>
    </div>

    <div class="divide-10"></div>
    <h3 class="sub-header">Reportes de calificaciones finales por grado</h3>

    <div class="col-md-12">
        <div class="form-group col-md-3">
            <label for="ddlAnio">Grado:</label>
            <asp:DropDownList ID="ddlGradoFinal" CssClass="form-control" runat="server">
                <%-- Grados --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-md-9">
            <div class="divide-25"></div>
            <asp:Button ID="btnReporteFinal" CssClass="btn btn-primary" runat="server" Text="Generar Reporte" OnClick="btnReporteFinal_Click" />
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

