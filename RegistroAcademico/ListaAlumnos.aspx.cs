﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class listaAlumnos : System.Web.UI.Page
{
    Alumnos Alumnos = new Alumnos();

    protected void dgvBind()
    {
        DataSet ds = new DataSet();

        if (!IsPostBack || (IsPostBack && txtBusqueda.Text == ""))
        {
            ds = Alumnos.Listado();
        }
        else
        {
            ds = Alumnos.Listado(txtBusqueda.Text);
        }
        
        gdvAlumnos.DataSource = ds;
        gdvAlumnos.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        dgvBind();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        
    }
}