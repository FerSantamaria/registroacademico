﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaAsignaturasProfesor.aspx.cs" Inherits="ListaAsignaturasProfesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Asignaturas</h2>

    <h3 class="sub-header">Listado</h3>

    <div class="form-group col-md-12">
        <h5><strong>Grado:</strong> <asp:Label ID="lblGrado" runat="server" Text="Kinder 4"></asp:Label></h5>
        <h5><strong>Sección:</strong> <asp:Label ID="lblSeccion" runat="server" Text="A"></asp:Label></h5>
    </div>

    <div class="col-md-8">
        <div class="table-responsive" id="tblListaUsuarios">
            <asp:GridView ID="gdvAsignaturas" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="idMateria"  ItemStyle-Width="10%" HeaderText="NIE"/>
                    <asp:BoundField DataField="Nombre"  ItemStyle-Width="50%" HeaderText="Nombre"/>
                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

