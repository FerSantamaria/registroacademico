﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditarGrado : System.Web.UI.Page
{
    Materia Materia = new Materia();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];

            try
            {
                Materia.Id = Convert.ToInt32(id);

                if (id != null && Materia.Datos())
                {
                    txtNombre.Text = Materia.Nombre;
                    Session["editMat"] = Materia;
                }
                else
                {
                    Session["editMat"] = null;
                    lblMensaje.Text = "Asignatura no encontrado. ";
                    panelSuccess.CssClass = "alert alert-danger";
                    panelSuccess.Visible = true;
                }
            }
            catch (FormatException)
            {
                Session["editMat"] = null;
                lblMensaje.Text = "Asignatura no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["editMat"] != null)
        {
            Materia = (Materia)Session["editMat"];

            Materia.Nombre = txtNombre.Text;

            if (Materia.Actualizar() > 0)
            {
                lblMensaje.Text = "Actualización exitosa. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }
        }
    }
}