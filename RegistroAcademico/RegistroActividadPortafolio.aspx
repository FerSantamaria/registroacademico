﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="RegistroActividadPortafolio.aspx.cs" Inherits="RegistroActividad" %>
<%@ PreviousPageType VirtualPath="~/ListaActividadesPortafolio.aspx" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2 class="header">Portafolio</h2>
    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaEmpleados.aspx">lista</a>
    </asp:Panel>
    
    <h3 class="sub-header">Registro Nueva Actividad</h3>

    <asp:Label ID="testLabel" runat="server"></asp:Label>

    <div class="form-group col-md-12">
        <h5><strong>Período:</strong> <asp:Label ID="lblPeriodo" runat="server" Text="01 2018"></asp:Label></h5>
        <h5><strong>Matería:</strong> <asp:Label ID="lblMateria" runat="server" Text="Matemática"></asp:Label></h5>
        <h5><strong>Grado:</strong> <asp:Label ID="lblGrado" runat="server" Text="Kinder 4"></asp:Label></h5>
        <h5><strong>Sección:</strong> <asp:Label ID="lblSeccion" runat="server" Text="A"></asp:Label></h5>
    </div>

    <div class="form-group col-md-12">
        <label for="txtTitulo">Título</label>
        <asp:TextBox ID="txtTitulo" CssClass="form-control" runat="server"></asp:TextBox>
    </div>

    <div class="form-group col-md-12">
        <label for="txtDescripcion">Descripción</label>
        <asp:TextBox ID="txtDescripcion" rows="3" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
    </div>

    <div class="form-group col-md-2">
        <label for="txtPorcentaje">Porcentaje</label>
        <asp:TextBox ID="txtPorcentaje" CssClass="form-control" runat="server" TextMode="Number"></asp:TextBox>
    </div>

    <div class="form-group col-md-10">
        <div class="divide-25"></div>
        <asp:Button ID="btnRegistrar" CssClass="btn btn-success pull-right" runat="server" Text="Registrar Actividad" OnClick="btnRegistrar_Click" />
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

