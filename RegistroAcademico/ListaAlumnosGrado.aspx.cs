﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaAlumnosGrado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Alumnos alumnos = new Alumnos();
        //<-- DATOS PRUEBA DISEÑO -->
        string grado = Request.QueryString["grado"];
        string seccion = Request.QueryString["seccion"];

        DataTable datos = new DataTable();
        datos.Columns.Add("NIE");
        datos.Columns.Add("Nombre");

        DataSet dt = alumnos.ListadoAlumnoGrado(grado, seccion);

        gdvAlumnos.DataSource = dt;
        gdvAlumnos.DataBind();

        lblGrado.Text = grado;
        lblSeccion.Text = seccion;
        
    }
}