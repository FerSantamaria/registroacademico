﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaAlumnos.aspx.cs" Inherits="listaAlumnos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Alumnos</h2>

    <h3 class="sub-header">Nuevo alumno</h3>
    <div class="col-md-12">
        <a class="btn btn-primary" href="RegistroAlumno.aspx">Registrar nuevo alumno</a>
    </div> 

    <div class="divide-10"></div>

    <h3 class="sub-header">Búsqueda</h3>
    <div class="form-group col-xs-10 col-md-3">
        <asp:Label ID="Label1" runat="server" Text="NIE o nombre" CssClass="sr-only" for="txtBusqueda"></asp:Label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-users"></i></div>
            <asp:TextBox ID="txtBusqueda" CssClass="form-control" runat="server" placeholder="NIE o nombre"></asp:TextBox>
        </div>
    </div>
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary col-xs-2 col-md-1" OnClick="btnBuscar_Click" />

    <div class="divide-10"></div>

    <h3 class="sub-header">Listado de alumnos</h3>
    <div class="table-responsive" id="tblListaUsuarios">
        <asp:GridView ID="gdvAlumnos" runat="server" AutoGenerateColumns="False" DataKeyNames="NIE" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="NIE"  HeaderText="NIE"/>
                <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                <asp:TemplateField HeaderText="Opciones" ShowHeader="False">
                    <ItemTemplate> 
                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-sm btn-success" NavigateUrl='<%# "RegistroMatriculaAlumno.aspx?Id=" + DataBinder.Eval(Container.DataItem, "IdAlumno") %>'>Registrar Matricula</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-sm btn-info" NavigateUrl='<%# "EditarAlumno.aspx?Id=" + DataBinder.Eval(Container.DataItem, "IdAlumno") %>'>Editar</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink3" runat="server" CssClass="btn btn-sm btn-danger" NavigateUrl='<%# "EliminarAlumno.aspx?Id=" + DataBinder.Eval(Container.DataItem, "IdAlumno") %>'>Eliminar</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Sin registros</EmptyDataTemplate>
        </asp:GridView>
    </div>

</asp:Content>

