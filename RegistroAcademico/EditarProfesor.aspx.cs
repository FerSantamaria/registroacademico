﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditarProfesor : System.Web.UI.Page
{
    Profesor Profesor = new Profesor();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];
            Profesor.Id = id;

            if (id != null && Profesor.Datos())
            {
                lblCodigo.Text = Profesor.Id;
                lblNombre.Text = Profesor.Nombres + " " + Profesor.Apellido1 + " " + Profesor.Apellido2;
                txtTitulo.Text = Profesor.Titulo;
                txtEspecialidad.Text = Profesor.Especialidad;
                Session["editProf"] = Profesor;
            }
            else
            {
                Session["editProf"] = null;
                lblMensaje.Text = "Profesor no encontrado/registrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["editProf"] != null)
        {
            Profesor = (Profesor)Session["editProf"];

            Profesor.Titulo = txtTitulo.Text;
            Profesor.Especialidad = txtEspecialidad.Text;

            if (Profesor.Actualizar() > 0)
            {
                panelSuccess.CssClass = "alert alert-success";
                lblMensaje.Text = "Actualización exitosa. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }
        }
    }
}