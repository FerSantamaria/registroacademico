﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="EditarAsignatura.aspx.cs" Inherits="EditarGrado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Asignaturas</h2>
    <h3>Editar Asignatura</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaAsignaturas.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nombre" ControlToValidate="txtNombre" Display="None"></asp:RequiredFieldValidator>

    <div class="divide-10"></div>

    <h4 class="sub-header">Información de la Asignatura</h4>
    <div class="form-group col-sm-12">
        <label for="txtNombre">Nombre:</label>
        <asp:TextBox ID="txtNombre" CssClass="form-control" runat="server"></asp:TextBox>
    </div>

    <div class="divide-20"></div>

    <div class="col-md-12">
        <asp:Button ID="btnGuardar" CssClass="btn btn-success pull-right" runat="server" Text="Actualizar Asignatura" OnClick="btnGuardar_Click"/>
    </div>

    <div class="divide-20"></div>

</asp:Content>

