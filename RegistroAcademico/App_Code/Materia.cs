﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Materia
/// </summary>
public class Materia
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public int Id { get; set; }
    public string Nombre { get; set; }

    public Materia()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public DataSet Listado()
    {
        string str = "SELECT * FROM Materia";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public DataSet Listado(string busqueda)
    {
        string str = "SELECT * FROM Materia " +
                        "WHERE Nombre LIKE '%' + @bus + '%'" +
                        "OR idMateria LIKE '%' + @bus + '%'";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@bus", busqueda);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public int Crear()
    {
        string str = "INSERT INTO Materia VALUES(@nom)";
        
        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        
        cmd.Parameters.AddWithValue("@nom", Nombre);

        int filas = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public int Actualizar()
    {
        string str = "UPDATE Materia SET Nombre=@nom WHERE idMateria=@Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@nom", Nombre);

        int filas = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public bool Datos()
    {
        bool res = false;
        string str = "SELECT * FROM Materia WHERE idMateria = @Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["idMateria"]);
                Nombre = Convert.ToString(dr["Nombre"]);
                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }
}