﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for Departamentos
/// </summary>
public class Departamentos
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public Departamentos()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable departamentos()
    {
        string str = "select ID, DepName from DEPSV";

        con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        DataTable dt = new DataTable();
        dt.Load(cmd.ExecuteReader());
        con.Close();

        return dt;
    }

    public DataTable municipios(int idDep)
    {
        string str = "select mun.ID, mun.MunName from MUNSV mun left join DEPSV dep on dep.ID = mun.DEPSV_ID where dep.ID = " + idDep;

        con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        DataTable dt = new DataTable();
        dt.Load(cmd.ExecuteReader());
        con.Close();

        return dt;
    }
}