﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;


/// <summary>
/// Summary description for Tareas
/// </summary>
public class Tareas
{
    public int idMateria { get; set; }
    public int idPeriodo { get; set; }
    public string tarea { get; set; }
    public string descripcion { get; set; }
    public int ponderacion { get; set; }
    public int idGrado { get; set; }
    public int idSeccion { get; set; }

    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public Tareas()
    {
    }

    public DataTable ListaPeriodo()
    {
        string str = "select per.idPeriodo, per.Nombre as Periodo from Periodo per, Anio an where an.anio = year(getdate())";

        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt;
    }

    public DataTable ListaMaterias()
    {
        string str = "select * from Materia";

        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt;
    }

    public virtual int Crear()
    {
        string str = "INSERT INTO detalleTarea VALUES(@materia, @periodo, @tarea, @descripcion, @ponderacion, @grado, @seccion)";


        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@materia", idMateria);
        cmd.Parameters.AddWithValue("@periodo", idPeriodo);
        cmd.Parameters.AddWithValue("@tarea", tarea);
        cmd.Parameters.AddWithValue("@descripcion", descripcion);
        cmd.Parameters.AddWithValue("@ponderacion", ponderacion);
        cmd.Parameters.AddWithValue("@grado", idGrado);
        cmd.Parameters.AddWithValue("@seccion", idSeccion);

        int filas = cmd.ExecuteNonQuery(); //Registro de Tarea

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public virtual DataTable ListadoTareasGrado(int grado, int seccion)
    {
        string str = "select idTarea, tarea as Titulo, descripcion as Descripcion, ponderacion as Porcentaje " +
            "from detalleTarea where idGrado = @grado and idSeccion = @seccion";

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@grado", grado);
        cmd.Parameters.AddWithValue("@seccion", seccion);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt;
    }

    public virtual DataTable DetalleTarea(int id)
    {
        string str = "select * from infoTarea where id = @idTarea";

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@idTarea", id);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt;
    }

    public virtual string getPeriodo(int id)
    {
        string str = "select Nombre + ' ' + cast(year(getdate()) as varchar(4)) as Periodo from Periodo where idPeriodo = @id";

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@id", id);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt.Rows[0][0].ToString();
    }

    public virtual string getMateria(int id)
    {
        string str = "select Nombre from Materia where idMateria = @id";

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@id", id);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt.Rows[0][0].ToString();
    }

    public virtual string getGrado(int id)
    {
        string str = "select Nombre from Grado where idGrado = @id";

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@id", id);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt.Rows[0][0].ToString();
    }

    public virtual string getSeccion(int id)
    {
        string str = "select Nombre from Seccion where idSeccion = @id";

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@id", id);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt.Rows[0][0].ToString();
    }

    public virtual int ingresarNotas(int tarea, int matricula, float nota)
    {
        string str = "exec ingresoNotas @tarea, @matricula, @nota";


        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@tarea", tarea);
        cmd.Parameters.AddWithValue("@matricula", matricula);
        cmd.Parameters.AddWithValue("@nota", nota);

        int reult = cmd.ExecuteNonQuery(); //Registro de Nota

        if (con.State == ConnectionState.Open) con.Close();

        return reult;
    }

    public virtual DataTable notasAlumnos(int tarea)
    {
        string str = "select alm.NIE, alm.nombres + ' ' + alm.apellido1 + ' ' + alm.apellido2 as nombre, ntt.nota as nota " +
            "from notaTarea ntt left join Matricula mat on ntt.idMatricula = mat.idMatricula " +
            "left join Alumno alm on alm.idAlumno = mat.idAlumno " +
            "where ntt.idTarea = @idTarea";

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@idTarea", tarea);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt;
    }
}