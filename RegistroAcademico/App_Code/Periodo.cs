﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Descripción breve de Periodo
/// </summary>
public class Periodo
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public List<ListItem> Periodos()
    {
        List<ListItem> Periodos = new List<ListItem>();

        string str = "SELECT * FROM Periodo ORDER BY idPeriodo ASC";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
                Periodos.Add(new ListItem(Convert.ToString(dr["Nombre"]), Convert.ToString(dr["idPeriodo"])));
        }

        if (con.State == ConnectionState.Open) con.Close();

        return Periodos;
    }
}