﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

/// <summary>
/// Descripción breve de Grado
/// </summary>
public class Grado
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public int Id { get; set; }
    public string Nombre { get; set; }
    public List<ListItem> Secciones { get; set; }
    public List<int> Materias { get; set; }

    public Grado()
    {
        Secciones = new List<ListItem>();
        Materias = new List<int>();
    }

    public DataSet Listado()
    {
        string str = "SELECT * FROM Grado";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public DataSet Listado(string busqueda)
    {
        string str = "SELECT * FROM Grado " +
                        "WHERE Nombre LIKE '%' + @bus + '%'" +
                        "OR idGrado LIKE '%' + @bus + '%'";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@bus", busqueda);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public int Crear(int Anio)
    {
        string str = "INSERT INTO Grado OUTPUT INSERTED.idGrado VALUES(@nom)";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@nom", Nombre);

        Id = (int)cmd.ExecuteScalar();
        int filas = CrearSeccionesGrado(Anio);

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    private int CrearSeccionesGrado(int Anio)
    {
        string str = "INSERT INTO DetalleGradoSeccion VALUES(@IdSeccion, @IdGrado, @anio)";
        int filas = 0;

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        foreach (var item in Secciones)
        {
            cmd = new SqlCommand(str, con);
            cmd.Parameters.AddWithValue("@IdSeccion", item.Value);
            cmd.Parameters.AddWithValue("@IdGrado", Id);
            cmd.Parameters.AddWithValue("@anio", Anio);
            filas += cmd.ExecuteNonQuery();
        }

        if (con.State == ConnectionState.Open) con.Close();

        return filas == Secciones.Count ? 1 : 0;
    }

    public bool Datos(int Anio)
    {
        bool res = false;
        string str = "SELECT * FROM Grado WHERE idGrado = @Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["idGrado"]);
                Nombre = Convert.ToString(dr["Nombre"]);
                dr.Close();
                SeccionesGrado(Anio);
                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }

    public List<ListItem> Grados()
    {
        List<ListItem> Grados = new List<ListItem>();

        string str = "SELECT * FROM Grado ORDER BY Nombre ASC";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
                Grados.Add(new ListItem(Convert.ToString(dr["Nombre"]), Convert.ToString(dr["idGrado"])));
        }

        if (con.State == ConnectionState.Open) con.Close();

        return Grados;
    }

    public void SeccionesGrado(int Anio)
    {
        Secciones = new List<ListItem>();

        string str = "SELECT s.* FROM Seccion s " +
                "INNER JOIN DetalleGradoSeccion d ON s.idSeccion = d.idSeccion " +
                "WHERE d.idGrado = @IdGrado AND d.anioLectivo = @anio";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@IdGrado", Id);
        cmd.Parameters.AddWithValue("@anio", Anio);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
                Secciones.Add(new ListItem(Convert.ToString(dr["Nombre"]), Convert.ToString(dr["idSeccion"])));
            dr.Close();
        }

        if (con.State == ConnectionState.Open) con.Close();
    }

    public DataTable ListaGrados()
    {
        DataTable Grados = new DataTable();

        string str = "select gr.idGrado, sec.idSeccion, gr.Nombre, sec.Nombre from grado gr " +
            "left join DetalleGradoSeccion dgs on dgs.idGrado = gr.idGrado " +
            "left join Seccion sec on sec.idSeccion = dgs.idSeccion " +
            "where dgs.anioLectivo = year(getdate())";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(Grados);

        if (con.State == ConnectionState.Open) con.Close();

        return Grados;
    }

    private void ResetSeccionesGrado(int Anio)
    {
        string str = "DELETE FROM DetalleGradoSeccion WHERE idGrado=@IdGrado AND anioLectivo=@anio";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@IdGrado", Id);
        cmd.Parameters.AddWithValue("@anio", Anio);
        cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();
    }

    public int Actualizar(int Anio)
    {
        ResetSeccionesGrado(Anio);

        string str = "UPDATE Grado SET Nombre=@nom WHERE idGrado=@Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@nom", Nombre);

        int filas = cmd.ExecuteNonQuery();
        filas += CrearSeccionesGrado(Anio);

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public DataSet HistoricoMateriasGrado()
    {
        string str = "SELECT m.Nombre, d.anioLectivo FROM Materia m " +
                        "INNER JOIN DetalleGradoMateria d ON d.idMateria = m.idMateria " +
                        "WHERE d.idGrado = @Id ";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public DataSet MateriasGrado(int Anio)
    {
        string str = "SELECT m.idMateria, m.Nombre FROM Materia m " +
                        "INNER JOIN DetalleGradoMateria d ON d.idMateria = m.idMateria " +
                        "WHERE d.idGrado = @Id AND anioLectivo=@Anio";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@Anio", Anio);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    private void ResetMateriasGrado(int Anio)
    {
        string str = "DELETE FROM DetalleGradoMateria WHERE idGrado=@IdGrado AND anioLectivo=@anio";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@IdGrado", Id);
        cmd.Parameters.AddWithValue("@anio", Anio);
        cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();
    }

    public int CrearMateriasGrado(int Anio)
    {
        ResetMateriasGrado(Anio);

        string str = "INSERT INTO DetalleGradoMateria VALUES(@IdGrado, @IdMateria, @anio)";
        int filas = 0;

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        foreach (var item in Materias)
        {
            cmd = new SqlCommand(str, con);
            cmd.Parameters.AddWithValue("@IdGrado", Id);
            cmd.Parameters.AddWithValue("@IdMateria", item);
            cmd.Parameters.AddWithValue("@anio", Anio);
            filas += cmd.ExecuteNonQuery();
        }

        if (con.State == ConnectionState.Open) con.Close();

        return filas == Materias.Count ? 1 : 0;
    }
}