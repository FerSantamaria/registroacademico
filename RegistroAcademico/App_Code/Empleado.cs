﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Descripción breve de Administrativo
/// </summary>
public class Empleado
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public string Id { get; set; }
    public string Nombres { get; set; }
    public string Apellido1 { get; set; }
    public string Apellido2 { get; set; }
    public string Direccion { get; set; }
    public string Telefono { get; set; }
    public int Cargo { get; set; }
    public Usuario Usuario { get; set; }

    public Empleado()
    {
        Usuario = new Usuario();
    }

    public virtual DataSet Listado()
    {
        string str = "SELECT idEmpleado, nombres + ' ' + Apellido1 AS Nombre, " +
            "Cargo = CASE cargo " +
                "WHEN 1 THEN 'Profesor' " +
                "WHEN 2 THEN 'Dirección' " +
                "WHEN 3 THEN 'Secretaría' " +
            "END " +
            "FROM Empleado";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public DataSet Listado(string busqueda)
    {
        string str = "SELECT idEmpleado, nombres + ' ' + Apellido1 AS Nombre, " +
            "Cargo = CASE cargo " +
                "WHEN 1 THEN 'Profesor' " +
                "WHEN 2 THEN 'Dirección' " +
                "WHEN 3 THEN 'Secretaría' " +
            "END " +
            "FROM Empleado " +
            "WHERE nombres LIKE '%' + @bus + '%'" +
            "OR Apellido1 LIKE '%' + @bus + '%'" +
            "OR Apellido2 LIKE '%' + @bus + '%'" +
            "OR idEmpleado LIKE '%' + @bus + '%'";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@bus", busqueda);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public virtual int Crear()
    {
        string str = "INSERT INTO Empleado VALUES(@Id, @nom, @ape1, @ape2, @dir, @tel, @cargo)";

        Usuario.Id = Id;
        Usuario.Tipo = Cargo;

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@nom", Nombres);
        cmd.Parameters.AddWithValue("@ape1", Apellido1);
        cmd.Parameters.AddWithValue("@ape2", Apellido2);
        cmd.Parameters.AddWithValue("@dir", Direccion);
        cmd.Parameters.AddWithValue("@tel", Telefono);
        cmd.Parameters.AddWithValue("@cargo", Cargo);

        int filas = cmd.ExecuteNonQuery(); //Registro de empleado
        filas += Usuario.Crear(); //Creación de usuario

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public virtual int Actualizar()
    {
        string str = "UPDATE Empleado SET nombres=@nom, Apellido1=@ape1, Apellido2=@ape2, Direccion=@dir, Telefono=@tel, cargo=@cargo " +
            "WHERE idEmpleado=@Id";

        Usuario.Id = Id;
        Usuario.Tipo = Cargo;

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@nom", Nombres);
        cmd.Parameters.AddWithValue("@ape1", Apellido1);
        cmd.Parameters.AddWithValue("@ape2", Apellido2);
        cmd.Parameters.AddWithValue("@dir", Direccion);
        cmd.Parameters.AddWithValue("@tel", Telefono);
        cmd.Parameters.AddWithValue("@cargo", Cargo);
        cmd.Parameters.AddWithValue("@Id", Id);

        int filas = cmd.ExecuteNonQuery(); //Registro de empleado
        filas += Usuario.Actualizar(); //Actualizar tipo de usuario (si fue cambiado)

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public virtual bool Datos()
    {
        bool res = false;
        string str = "SELECT * FROM Empleado WHERE idEmpleado = @Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToString(dr["idEmpleado"]);
                Nombres = Convert.ToString(dr["nombres"]);
                Apellido1 = Convert.ToString(dr["Apellido1"]);
                Apellido2 = Convert.ToString(dr["Apellido2"]);
                Direccion = Convert.ToString(dr["Direccion"]);
                Telefono = Convert.ToString(dr["Telefono"]);
                Cargo = Convert.ToInt32(dr["cargo"]);

                Usuario.Id = Id;
                Usuario.Datos();

                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }

    public bool TieneRegistros()
    {
        bool res = false;
        string str = "SELECT * FROM DetalleMateriaProfesor WHERE idProfesor = @Id";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            res = dr.Read();
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }

    public int Eliminar()
    {
        string str = "DELETE FROM Empleado WHERE idEmpleado = @Id";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@Id", Id);
        int fila = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return fila;
    }
}