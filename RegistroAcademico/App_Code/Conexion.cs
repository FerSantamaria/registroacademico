﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Conexion
/// </summary>
public class Conexion
{
    //private string ConStr = "Data Source=MULLER\\SQLEXPRESS;Integrated Security=true;Initial Catalog=colegio";
    //private string ConStr = "Data Source=Marvin\\SQLEXPRESS;Initial Catalog=colegio;Integrated Security=True";
    //private string ConStr = "Data Source=LAPTOP-9OHTC3AH\\SQLEXPRESS;Initial Catalog=colegio;Integrated Security=True";
    //private string ConStr = "Data Source=AUGUSTO-PC2;Integrated Security=true;Initial Catalog=colegio";
    private string ConStr = "Data Source=localhost;Integrated Security=true;Initial Catalog=colegio";
    private SqlConnection con;

    public Conexion()
    {
        con = new SqlConnection(ConStr);
    }

    public SqlConnection GetConexion()
    {
        return con;
    }
}