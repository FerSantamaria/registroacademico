﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Descripción breve de Matricula
/// </summary>
public class Matricula
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public int Id { get; set; }
    public string IdAlumno { get; set; }
    public int IdGrado { get; set; }
    public int IdSeccion { get; set; }
    public int AnioLectivo { get; set; }
    public bool Repite { get; set; }

    public Matricula()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public DataSet ListaMatriculasAlumno()
    {
        string str = "SELECT m.anioLectivo, g.Nombre AS grado, s.Nombre AS seccion, m.repite " +
                        "FROM Matricula m " +
                        "INNER JOIN Grado g ON g.idGrado = m.idGrado " +
                        "INNER JOIN Seccion s ON s.idSeccion = m.idSeccion " +
                        "WHERE idAlumno = @IdAlumno";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@IdAlumno", IdAlumno);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public int Crear()
    {
        string str = "INSERT INTO Matricula VALUES(@IdAlumno, @IdGrado, @IdSeccion, @AnioLectivo, @Repite)";
        
        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        
        cmd.Parameters.AddWithValue("@IdAlumno", IdAlumno);
        cmd.Parameters.AddWithValue("@IdGrado", IdGrado);
        cmd.Parameters.AddWithValue("@IdSeccion", IdSeccion);
        cmd.Parameters.AddWithValue("@AnioLectivo", AnioLectivo);
        cmd.Parameters.AddWithValue("@Repite", Repite ? 1 : 0);

        int filas = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public bool Existe()
    {
        bool res = false;
        string str = "SELECT * FROM Matricula WHERE idAlumno=@Id AND anioLectivo=@Anio";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", IdAlumno);
        cmd.Parameters.AddWithValue("@Anio", AnioLectivo);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["idMatricula"]);
                IdGrado = Convert.ToInt32(dr["idGrado"]);
                IdSeccion = Convert.ToInt32(dr["idSeccion"]);
                Repite = Convert.ToInt32(dr["repite"]) == 1 ? true : false;
                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }
}