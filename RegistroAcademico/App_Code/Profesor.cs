﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

/// <summary>
/// Descripción breve de Profesor
/// </summary>
public class Profesor : Empleado
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();
    
    public string Titulo { get; set; }
    public string Especialidad { get; set; }

    public Profesor()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public override DataSet Listado()
    {
        string str = "SELECT Empleado.idEmpleado, Empleado.nombres + ' ' + Empleado.Apellido1 as Nombre, " +
            "Registrado = CASE WHEN EXISTS(SELECT Profesor.especialidad FROM Profesor WHERE idProfesor = Empleado.idEmpleado) THEN 1 ELSE 0 END " +
            "FROM Empleado WHERE Empleado.cargo=1";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);

        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public override int Crear()
    {
        string str = "INSERT INTO Profesor VALUES(@Id, @tit, @esp)";
        
        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@tit", Titulo);
        cmd.Parameters.AddWithValue("@esp", Especialidad);

        int filas = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public List<ListItem> ProfsNoReg()
    {
        List<ListItem> Profs = new List<ListItem>();

        string str = "SELECT Empleado.idEmpleado, Empleado.nombres + ' ' + Empleado.Apellido1 as Nombre " +
            "FROM Empleado " +
            "LEFT JOIN Profesor ON Profesor.idProfesor = Empleado.idEmpleado " +
            "WHERE Profesor.idProfesor IS NULL AND Empleado.cargo = 1";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
            {
                Profs.Add(new ListItem(Convert.ToString(dr["Nombre"]), Convert.ToString(dr["idEmpleado"])));
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return Profs;
    }

    public override bool Datos()
    {
        bool res = false;
        string str = "SELECT e.idEmpleado, e.nombres, e.Apellido1, e.Apellido2, p.titulo, p.especialidad " +
                        "FROM Empleado e " +
                        "INNER JOIN Profesor p ON p.idProfesor = e.idEmpleado " +
                        "WHERE p.idProfesor = @Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToString(dr["idEmpleado"]);
                Nombres = Convert.ToString(dr["nombres"]);
                Apellido1 = Convert.ToString(dr["Apellido1"]);
                Apellido2 = Convert.ToString(dr["Apellido2"]);
                Titulo = Convert.ToString(dr["titulo"]);
                Especialidad = Convert.ToString(dr["especialidad"]);

                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }

    public override int Actualizar()
    {
        string str = "UPDATE Profesor SET titulo=@tit, especialidad=@espe WHERE idProfesor=@Id";
        
        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@tit", Titulo);
        cmd.Parameters.AddWithValue("@espe", Especialidad);
        cmd.Parameters.AddWithValue("@Id", Id);

        int filas = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public DataSet MateriasProfesor()
    {
        string str = "SELECT m.Nombre, g.Nombre AS Grado, s.Nombre AS Seccion, d.anioLectivo " +
                        "FROM DetalleMateriaProfesor d " + 
                        "INNER JOIN Grado g ON g.idGrado = d.idGrado " + 
                        "INNER JOIN Seccion s ON s.idSeccion = d.idSeccion " +
                        "INNER JOIN Materia m ON m.idMateria = d.idMateria " +
                        "WHERE d.idProfesor = @Id";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@Id", Id);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);

        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    private void ResetMateriasProfesor(int Anio)
    {
        string str = "DELETE FROM DetalleMateriaProfesor WHERE idProfesor=@Id AND anioLectivo=@anio";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@anio", Anio);
        cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();
    }

    public int CrearMateriasProfesor(int Anio, int IdGrado, int IdSeccion, List<int> Materias)
    {
        ResetMateriasProfesor(Anio);

        string str = "INSERT INTO DetalleMateriaProfesor VALUES(@IdProf, @IdMateria, @IdGrado, @IdSeccion, @anio)";
        int filas = 0;

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        foreach (var item in Materias)
        {
            cmd = new SqlCommand(str, con);
            cmd.Parameters.AddWithValue("@IdProf", Id);
            cmd.Parameters.AddWithValue("@IdMateria", item);
            cmd.Parameters.AddWithValue("@IdGrado", IdGrado);
            cmd.Parameters.AddWithValue("@IdSeccion", IdSeccion);
            cmd.Parameters.AddWithValue("@anio", Anio);
            filas += cmd.ExecuteNonQuery();
        }

        if (con.State == ConnectionState.Open) con.Close();

        return filas == Materias.Count ? 1 : 0;
    }
}