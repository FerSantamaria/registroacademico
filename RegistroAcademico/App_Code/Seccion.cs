﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Descripción breve de Seccion
/// </summary>
public class Seccion
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public int Id { get; set; }
    public string Nombre { get; set; }

    public Seccion()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public DataSet Listado()
    {
        string str = "SELECT * FROM Seccion";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }
    
    public int Crear()
    {
        string str = "INSERT INTO Seccion VALUES(@nom)";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@nom", Nombre);

        int filas = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public int Actualizar()
    {
        string str = "UPDATE Seccion SET Nombre=@nom WHERE idSeccion=@Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@nom", Nombre);

        int filas = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public bool Datos()
    {
        bool res = false;
        string str = "SELECT * FROM Seccion WHERE idSeccion = @Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["idSeccion"]);
                Nombre = Convert.ToString(dr["Nombre"]);
                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }

    public List<ListItem> Secciones()
    {
        List<ListItem> Secciones = new List<ListItem>();

        string str = "SELECT * FROM Seccion ORDER BY Nombre ASC";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
                Secciones.Add(new ListItem(Convert.ToString(dr["Nombre"]), Convert.ToString(dr["idSeccion"])));
        }

        if (con.State == ConnectionState.Open) con.Close();

        return Secciones;
    }
}