﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Descripción breve de Alumnos
/// </summary>
public class Alumnos
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public string Id { get; set; }
    public string Nombres { get; set; }
    public string Apellido1 { get; set; }
    public string Apellido2 { get; set; }
    public string NIE { get; set; }
    public string Direccion { get; set; }
    public DateTime FechaNacimiento { get; set; }
    public DateTime FechaIngreso { get; set; }
    public int Departamento { get; set; }
    public int Municipio { get; set; }
    public int Sexo { get; set; }
    public string Partida { get; set; }
    public int Folio { get; set; }
    public string Habitantes { get; set; }
    public string Responsable { get; set; }
    public string ResponsableDUI { get; set; }
    public string ResponsableProfesion { get; set; }
    public int MiembrosFamilia { get; set; }
    public int NoEscribenLeen { get; set; }
    public int EstadoCivilPadres { get; set; }
    public string ContactoEmergencia { get; set; }
    public string ParentescoEmergencia { get; set; }
    public string TelefonoEmergencia { get; set; }
    public string Religion { get; set; }
    public int Bautismo { get; set; }
    public int Comunion { get; set; }
    public int Confirmacion { get; set; }
    public List<string> Salud { get; set; }


    public Alumnos()
    {
        Salud = new List<string>();
    }

    public DataSet Listado()
    {
        string str = "SELECT idAlumno, NIE, nombres + ' ' + apellido1 + ' ' + apellido2 AS Nombre FROM Alumno";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }
    
    public DataSet Listado(string busqueda)
    {
        string str = "SELECT IdAlumno, NIE, nombres + ' ' + apellido1 + ' ' + apellido2 AS Nombre " +
                        "FROM Alumno " +
                        "WHERE nombres LIKE '%' + @bus + '%'" +
                        "OR apellido1 LIKE '%' + @bus + '%'" +
                        "OR apellido2 LIKE '%' + @bus + '%'" +
                        "OR NIE LIKE '%' + @bus + '%'";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@bus", busqueda);

        //System.Diagnostics.Debug.WriteLine(cmd.Parameters["@bus"].Value);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }

    public int registrarAlumno(string nie, string nombres, string apellido1, string apellido2, string departamento, string municipio, string partida, int folio, int sexo, string direccion, string habitantes, string responsablePersona, string responsableDui, string responsableProfesion, int miembrosFamilia, int familiaLecturaEscritura, int estadoCivil, string telefono, string fechaNacimiento, string fechaIngreso, string emergenciaContacto, string emergenciaParentesco, string religion, int bautismo, int confirmacion, int primeraComunion, string alergias)
    {
        string[] alergico = alergias.Split(',');
        string idAlumno = "";
        int ret = 0;

        string str = "EXEC [dbo].[nuevoAlumno] @nie, @nombres, @apellido1, @apellido2, @departamento, @municipio, @partida, @folio, @sexo, " +
            "@direccion, @habitantes, @responsablePersona, @responsableDui, @responsableProfesion, @miembrosFamilia, @familiaLecturaEscritura, @estadoCivil, @telefono, @fechaNacimiento, @fechaIngreso, @emergenciaContacto, " +
            "@emergenciaParentesco, @religion, @bautismo, @primeraComunion, @confirmacion";
        
        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand("nuevoAlumno", con);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@nie", nie);
        cmd.Parameters.AddWithValue("@nombres", nombres);
        cmd.Parameters.AddWithValue("@apellido1", apellido1);
        cmd.Parameters.AddWithValue("@apellido2", apellido2);
        cmd.Parameters.AddWithValue("@departamento", departamento);
        cmd.Parameters.AddWithValue("@municipio", municipio);
        cmd.Parameters.AddWithValue("@partida", partida);
        cmd.Parameters.AddWithValue("@folio", folio);
        cmd.Parameters.AddWithValue("@sexo", sexo);
        cmd.Parameters.AddWithValue("@direccion", direccion);
        cmd.Parameters.AddWithValue("@habitantes", habitantes);
        cmd.Parameters.AddWithValue("@responsablePersona", responsablePersona);
        cmd.Parameters.AddWithValue("@responsableDui", responsableDui);
        cmd.Parameters.AddWithValue("@responsableProfesion", responsableProfesion);
        cmd.Parameters.AddWithValue("@miembrosFamilia", miembrosFamilia);
        cmd.Parameters.AddWithValue("@familiaLecturaEscritura", familiaLecturaEscritura);
        cmd.Parameters.AddWithValue("@estadoCivil", estadoCivil);
        cmd.Parameters.AddWithValue("@telefono", telefono);
        cmd.Parameters.AddWithValue("@fechaNacimiento", fechaNacimiento);
        cmd.Parameters.AddWithValue("@fechaIngreso", fechaIngreso);
        cmd.Parameters.AddWithValue("@emergenciaContacto", emergenciaContacto);
        cmd.Parameters.AddWithValue("@emergenciaParentesco", emergenciaParentesco);
        cmd.Parameters.AddWithValue("@religion", religion);
        cmd.Parameters.AddWithValue("@bautismo", bautismo);
        cmd.Parameters.AddWithValue("@confirmacion", confirmacion);
        cmd.Parameters.AddWithValue("@primeraComunion", primeraComunion);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
            {
                idAlumno = Convert.ToString(dr["idAlumno"]);
            }
        }

        foreach(string alergia in alergico)
        {
            str = "insert into Salud values (@alergia, @idAlumno)";
            cmd = new SqlCommand(str, con);
            cmd.Parameters.AddWithValue("@alergia", alergia);
            cmd.Parameters.AddWithValue("@idAlumno", idAlumno);
            ret = cmd.ExecuteNonQuery();
        }

        if (con.State == ConnectionState.Open) con.Close();

        return ret;
    }

    public bool Datos()
    {
        bool res = false;
        string str = "SELECT Alumno.*, MUNSV.DEPSV_ID " +
            "FROM Alumno " +
            "INNER JOIN MUNSV ON Alumno.municipio = MUNSV.ID " +
            "WHERE idAlumno = @Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToString(dr["idAlumno"]);
                Nombres = Convert.ToString(dr["nombres"]);
                Apellido1 = Convert.ToString(dr["apellido1"]);
                Apellido2 = Convert.ToString(dr["apellido2"]);
                NIE = Convert.ToString(dr["NIE"]);
                FechaIngreso = Convert.ToDateTime(dr["fechaIngreso"]);
                FechaNacimiento = Convert.ToDateTime(dr["fechaNacimiento"]);
                Direccion = Convert.ToString(dr["direccion"]);
                Departamento = Convert.ToInt32(dr["DEPSV_ID"]);
                Municipio = Convert.ToInt32(dr["municipio"]);
                Partida = Convert.ToString(dr["partida"]);
                Folio = Convert.ToInt32(dr["folio"]);
                Sexo = Convert.ToInt32(dr["NIE"]);
                Habitantes = Convert.ToString(dr["habitantes"]);
                Responsable = Convert.ToString(dr["responsablePersona"]);
                ResponsableDUI = Convert.ToString(dr["responsableDui"]);
                ResponsableProfesion = Convert.ToString(dr["responsableProfesion"]);
                MiembrosFamilia = Convert.ToInt32(dr["miembrosFamilia"]);
                NoEscribenLeen = Convert.ToInt32(dr["familiarLecturaEscritura"]);
                EstadoCivilPadres = Convert.ToInt32(dr["estadoCivil"]);
                ContactoEmergencia = Convert.ToString(dr["emergenciaContact"]);
                ParentescoEmergencia = Convert.ToString(dr["emergenciaParentesco"]);
                TelefonoEmergencia = Convert.ToString(dr["telefono"]);
                Religion = Convert.ToString(dr["religion"]);
                Bautismo = Convert.ToInt32(dr["bautismo"]);
                Comunion = Convert.ToInt32(dr["primeraComunion"]);
                Confirmacion = Convert.ToInt32(dr["confirmacion"]);
                dr.Close();
                GetSalud();

                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }

    public void GetSalud()
    {
        Salud = new List<string>();
        string str = "SELECT * FROM Salud WHERE idAlumno=@Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
            {
                Salud.Add(Convert.ToString(dr["enfermedad"]));
            }
        }

        if (con.State == ConnectionState.Open) con.Close();
    }

    public void ResetSalud()
    {
        string str = "DELETE FROM Salud WHERE idAlumno=@Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();
    }

    public int Actualizar()
    {
        ResetSalud();

        string str = "UPDATE Alumno SET NIE=@nie, nombres=@nom, apellido1=@ape1, apellido2=@ape2, " +
            "municipio=@mun, partida=@par, folio=@fol, sexo=@sexo, direccion=@dir, habitantes=@hab, " +
            "responsablePersona=@resp, responsableDui=@dui, responsableProfesion=@prof, miembrosFamilia=@fam, " +
            "familiarLecturaEscritura=@lee, estadoCivil=@civil, fechaNacimiento=@fn, fechaIngreso=@fi, " +
            "emergenciaContact=@emer, emergenciaParentesco=@pare, telefono=@tel, religion=@reli, " +
            "bautismo=@bau, confirmacion=@conf, primeraComunion=@comu " +
            "WHERE idAlumno=@Id";
        
        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@nie", NIE);
        cmd.Parameters.AddWithValue("@nom", Nombres);
        cmd.Parameters.AddWithValue("@ape1", Apellido1);
        cmd.Parameters.AddWithValue("@ape2", Apellido2);
        cmd.Parameters.AddWithValue("@mun", Municipio);
        cmd.Parameters.AddWithValue("@par", Partida);
        cmd.Parameters.AddWithValue("@fol", Folio);
        cmd.Parameters.AddWithValue("@sexo", Sexo);
        cmd.Parameters.AddWithValue("@dir", Direccion);
        cmd.Parameters.AddWithValue("@hab", Habitantes);
        cmd.Parameters.AddWithValue("@resp", Responsable);
        cmd.Parameters.AddWithValue("@dui", ResponsableDUI);
        cmd.Parameters.AddWithValue("@prof", ResponsableProfesion);
        cmd.Parameters.AddWithValue("@fam", MiembrosFamilia);
        cmd.Parameters.AddWithValue("@lee", NoEscribenLeen);
        cmd.Parameters.AddWithValue("@civil", EstadoCivilPadres);
        cmd.Parameters.AddWithValue("@fn", FechaNacimiento);
        cmd.Parameters.AddWithValue("@fi", FechaIngreso);
        cmd.Parameters.AddWithValue("@emer", ContactoEmergencia);
        cmd.Parameters.AddWithValue("@pare", ParentescoEmergencia);
        cmd.Parameters.AddWithValue("@tel", TelefonoEmergencia);
        cmd.Parameters.AddWithValue("@reli", Religion);
        cmd.Parameters.AddWithValue("@bau", Bautismo);
        cmd.Parameters.AddWithValue("@conf", Confirmacion);
        cmd.Parameters.AddWithValue("@comu", Comunion);
        cmd.Parameters.AddWithValue("@Id", Id);

        int filas = cmd.ExecuteNonQuery();

        str = "INSERT INTO Salud VALUES (@alergia, @idAlumno)";
        foreach (string alergia in Salud)
        {
            cmd = new SqlCommand(str, con);
            cmd.Parameters.AddWithValue("@alergia", alergia);
            cmd.Parameters.AddWithValue("@idAlumno", Id);
            filas += cmd.ExecuteNonQuery();
        }

        if (con.State == ConnectionState.Open) con.Close();

        return filas;
    }

    public bool TieneRegistros()
    {
        bool res = false;
        string str = "SELECT * FROM Matricula WHERE idAlumno = @Id";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            res = dr.Read();
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }

    public int Eliminar()
    {
        string str = "DELETE FROM Alumno WHERE idAlumno = @Id";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);
        cmd.Parameters.AddWithValue("@Id", Id);
        int fila = cmd.ExecuteNonQuery();

        if (con.State == ConnectionState.Open) con.Close();

        return fila;
    }


    public DataSet ListadoAlumnoGrado(string grado, string seccion)
    {
        string str = "select alm.idAlumno, mat.idMatricula, alm.NIE, alm.nombres + ' ' + alm.apellido1 + ' ' + alm.apellido2 as Nombre " +
            "from Alumno alm left join Matricula mat on mat.idAlumno = alm.idAlumno where mat.idGrado = @grado and mat.idSeccion = @seccion and anioLectivo = @anio";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@grado", grado);
        cmd.Parameters.AddWithValue("@seccion", seccion);
        cmd.Parameters.AddWithValue("@anio", DateTime.Today.Year);

        //System.Diagnostics.Debug.WriteLine(cmd.Parameters["@bus"].Value);

        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (con.State == ConnectionState.Open) con.Close();

        return ds;
    }
}