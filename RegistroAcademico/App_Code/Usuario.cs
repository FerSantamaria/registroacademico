﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;

/// <summary>
/// Descripción breve de Usuario
/// </summary>
public class Usuario
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public string Id { get; set; }
    public string Contrasena { get; set; }
    public int Tipo { get; set; }

    public Usuario()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public int Crear()
    {
        string str = "INSERT INTO Usuario VALUES(@Id, @con, @tipo)";

        Contrasena = GenerarContrasena(8);

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@con", sha256(Contrasena));
        cmd.Parameters.AddWithValue("@tipo", Tipo);

        int fila = cmd.ExecuteNonQuery();
        if (con.State == ConnectionState.Open) con.Close();

        return fila;
    }

    public int Actualizar()
    {
        string str = "UPDATE Usuario SET tipo=@tipo WHERE idPersona=@Id";
        
        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@tipo", Tipo);

        int fila = cmd.ExecuteNonQuery();
        if (con.State == ConnectionState.Open) con.Close();

        return fila;
    }

    public bool Datos()
    {
        bool res = false;
        string str = "SELECT * FROM Usuario WHERE idPersona = @Id";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@Id", Id);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToString(dr["idPersona"]);
                Contrasena = Convert.ToString(dr["contraseña"]);
                Tipo = Convert.ToInt32(dr["tipo"]);
                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }


    public string GenerarContrasena(int length)
    {
        Random random = new Random();
        string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder result = new StringBuilder(length);
        for (int i = 0; i < length; i++)
        {
            result.Append(characters[random.Next(characters.Length)]);
        }
        return result.ToString();
    }

    public string sha256(string str)
    {
        var crypt = new System.Security.Cryptography.SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(str));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    public bool Autenticar()
    {
        bool res = false;
        string str = "SELECT * FROM Usuario WHERE idPersona=@usuario AND contraseña=@contra";

        if (con.State == ConnectionState.Closed) con.Open();
        SqlCommand cmd = new SqlCommand(str, con);

        cmd.Parameters.AddWithValue("@usuario", Id);
        cmd.Parameters.AddWithValue("@contra", sha256(Contrasena));

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            if (dr.Read())
            {
                Id = Convert.ToString(dr["idPersona"]);
                Contrasena = Convert.ToString(dr["contraseña"]);
                Tipo = Convert.ToInt32(dr["tipo"]);
                res = true;
            }
        }

        if (con.State == ConnectionState.Open) con.Close();

        return res;
    }
}