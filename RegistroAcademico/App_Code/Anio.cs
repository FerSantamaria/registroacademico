﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Descripción breve de Anio
/// </summary>
public class Anio
{
    static Conexion objCon = new Conexion();
    SqlConnection con = objCon.GetConexion();

    public int AnioLectivo { get; set; }

    public Anio()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public List<ListItem> Anios()
    {
        List<ListItem> Anios = new List<ListItem>();

        string str = "SELECT * FROM Anio";

        if (con.State == ConnectionState.Closed) con.Open();

        SqlCommand cmd = new SqlCommand(str, con);

        using (SqlDataReader dr = cmd.ExecuteReader())
        {
            while (dr.Read())
                Anios.Add(new ListItem(Convert.ToString(dr["Anio"]), Convert.ToString(dr["Anio"])));
        }

        if (con.State == ConnectionState.Open) con.Close();

        return Anios;
    }
}