﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="EditarAlumno.aspx.cs" Inherits="EditarAlumno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Alumnos</h2>
    <h3>Nuevo alumno</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaAlumnos.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />

    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="NIE" ControlToValidate="txtNIE" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Nombres" ControlToValidate="txtNombres" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Primer apellido" ControlToValidate="txtPrimerApellido" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Dirección" ControlToValidate="txtDireccion" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Departamento" ControlToValidate="ddlDepartamento" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Municipio" ControlToValidate="ddlMunicipio" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Fecha de nacimiento" ControlToValidate="calNacimiento" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Fecha de ingreso" ControlToValidate="calIngreso" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Sexo" ControlToValidate="ddlSexo" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Partida" ControlToValidate="txtPartida" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Vive con" ControlToValidate="txtVive" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Responsable" ControlToValidate="txtResponsable" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="DUI Responsable" ControlToValidate="txtDUI" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Estado civil padres" ControlToValidate="txtEstCivil" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Miembros de la familia" ControlToValidate="txtMiembros" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="No leen/escriben" ControlToValidate="txtNoLeen" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Teléfono de contacto" ControlToValidate="txtTelefono" Display="None"></asp:RequiredFieldValidator>


    <div class="divide-10"></div>

    <h4 class="sub-header">Información del Alumno</h4>
    <div class="form-group col-sm-3">
        <label for="txtNIE">NIE:</label>
        <asp:TextBox ID="txtNIE" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="clearfix"></div>
    <div class="form-group col-sm-12">
        <label for="txtNombres">Nombres:</label>
        <asp:TextBox ID="txtNombres" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtPrimerApellido">Primer Apellido:</label>
        <asp:TextBox ID="txtPrimerApellido" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtSegundoApellido">Segundo Apellido:</label>
        <asp:TextBox ID="txtSegundoApellido" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-12">
        <label for="txtDireccion">Dirección:</label>
        <asp:TextBox ID="txtDireccion" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-2">
        <label for="calNacimiento">Fecha de Nacimiento:</label>
        <asp:TextBox ID="calNacimiento" CssClass="form-control" TextMode="Date"  runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-2">
        <label for="calIngreso">Fecha de Ingreso:</label>
        <asp:TextBox ID="calIngreso" CssClass="form-control" TextMode="Date"  runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-4">
        <label for="ddlDepartamento">Departamento:</label>
        <asp:DropDownList ID="ddlDepartamento" CssClass="form-control" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <div class="form-group col-sm-4">
        <label for="ddlMunicipio">Municipio:</label>
        <asp:DropDownList ID="ddlMunicipio" CssClass="form-control" runat="server">
        </asp:DropDownList>
    </div>
    <div class="form-group col-sm-2">
        <label for="ddlSexo">Sexo:</label>
        <asp:DropDownList ID="ddlSexo" CssClass="form-control" runat="server">
            <asp:ListItem Value="1">Masculino</asp:ListItem>
            <asp:ListItem Value="2">Femenino</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="form-group col-sm-2">
        <label for="txtPartida">Partida N°:</label>
        <asp:TextBox ID="txtPartida" CssClass="form-control" TextMode="Number" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-2">
        <label for="txtFolio">Folio:</label>
        <asp:TextBox ID="txtFolio" CssClass="form-control" TextMode="Number" runat="server"></asp:TextBox>
    </div>

    <div class="divide-20"></div>

    <h4 class="sub-header">Información Familiar</h4>
    <div class="form-group col-sm-12">
        <label for="txtVive">Vive con:</label>
        <asp:TextBox ID="txtVive" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtResponsable">Persona Responsable:</label>
        <asp:TextBox ID="txtResponsable" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-3">
        <label for="txtDUI">DUI:</label>
        <asp:TextBox ID="txtDUI" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-3">
        <label for="txtProfResp">Profesión/Oficio:</label>
        <asp:TextBox ID="txtProfResp" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-3">
        <label for="txtEstCivil">Estado Civil Padres:</label>
        <asp:DropDownList ID="txtEstCivil" CssClass="form-control" runat="server">
            <asp:ListItem Value="1">Casados</asp:ListItem>
            <asp:ListItem Value="2">Divorciados</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="form-group col-sm-3">
        <label for="txtMiembros">Miembros de la familia:</label>
        <asp:TextBox ID="txtMiembros" CssClass="form-control" TextMode="Number" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-3">
        <label for="txtNoLeen">No leen/escriben:</label>
        <asp:TextBox ID="txtNoLeen" CssClass="form-control" TextMode="Number" runat="server"></asp:TextBox>
    </div>

    <div class="divide-20"></div>

    <h4 class="sub-header">Información Médica</h4>
    <div class="form-group col-sm-12">
        <label for="txtAlergias">Alérgico a (separar por comas):</label>
        <asp:TextBox ID="txtAlergias" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtContEmergencia">En caso de emergencia avisar a:</label>
        <asp:TextBox ID="txtContEmergencia" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-3">
        <label for="txtParentesco">Parentesco:</label>
        <asp:TextBox ID="txtParentesco" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-3">
        <label for="txtTelefono">Teléfono:</label>
        <asp:TextBox ID="txtTelefono" CssClass="form-control" runat="server"></asp:TextBox>
    </div>

    <div class="divide-20"></div>

    <h4 class="sub-header">Otra Información</h4>
    <div class="form-group col-sm-4">
        <label for="txtReligion">Religión:</label>
        <asp:TextBox ID="txtReligion" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-3">
        <asp:Panel ID="pnlSacramentos" runat="server">
            <label>Sacramentos:</label>
            <asp:CheckBoxList CssClass="checkboxlistformat" ID="CheckBoxList1" runat="server">
                <asp:ListItem Value="bautismo"> Bautismo</asp:ListItem>
                <asp:ListItem Value="primeraComunion"> Primera Comunión</asp:ListItem>
                <asp:ListItem Value="confirmacion">Confirmación</asp:ListItem>
            </asp:CheckBoxList>
        </asp:Panel>
    </div>

    <div class="divide-20"></div>

    <div class="col-md-12">
        <asp:Button ID="btnGuardar" CssClass="btn btn-success pull-right" runat="server" Text="Actualizar Alumno" OnClick="btnGuardar_Click" />
    </div>

    <div class="divide-20"></div>
</asp:Content>

