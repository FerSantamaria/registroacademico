﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ReporteFinalGrado.aspx.cs" Inherits="ReportePeriodoGrado" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Reportes</h2>
    <h3 class="sub-header">Reportes de calificaciones finales de grado</h3>

    <%--<div class="form-group col-md-12">
        <h4><strong>Grado:</strong> <asp:Label ID="lblGrado" runat="server" Text="Primer Grado"></asp:Label></h4>
        <h4><strong>Período:</strong> <asp:Label ID="lblPeriodo" runat="server" Text="Primer Grado"></asp:Label></h4>
    </div>--%>

    <div class="col-md-12">
        <rsweb:ReportViewer ID="rptPeriodo" runat="server" Width="100%" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" BackColor="#CCCCCC" ShowBackButton="False" SizeToReportContent="True">
            <LocalReport ReportPath="ReporteFinal.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="AlumnosMateriasGradosFinal" />
                </DataSources>
            </LocalReport>            
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="DatosTableAdapters.MateriasAlumnoGradoFinalTableAdapter" OnSelecting="ObjectDataSource1_Selecting">
            <SelectParameters>
                <asp:Parameter Name="anio" />
                <asp:Parameter Name="grado" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

