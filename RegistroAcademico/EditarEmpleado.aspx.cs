﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditarEmpleado : System.Web.UI.Page
{
    Empleado Empleado = new Empleado();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];
            Empleado.Id = id;

            if (id != null && Empleado.Datos())
            {
                txtNombres.Text = Empleado.Nombres;
                txtPrimerApellido.Text = Empleado.Apellido1;
                txtSegundoApellido.Text = Empleado.Apellido2;
                txtDireccion.Text = Empleado.Direccion;
                txtTelefono.Text = Empleado.Telefono;
                ddcargo.SelectedValue = Empleado.Cargo.ToString();
                Session["editEmp"] = Empleado;
            }
            else
            {
                Session["editEmp"] = null;
                lblMensaje.Text = "Empleado no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["editEmp"] != null)
        {
            Empleado = (Empleado)Session["editEmp"];

            Empleado.Nombres = txtNombres.Text;
            Empleado.Apellido1 = txtPrimerApellido.Text;
            Empleado.Apellido2 = txtSegundoApellido.Text;
            Empleado.Direccion = txtDireccion.Text;
            Empleado.Telefono = txtTelefono.Text;
            Empleado.Cargo = Convert.ToInt32(ddcargo.SelectedItem.Value);

            if (Empleado.Actualizar() > 1)
            {
                panelSuccess.CssClass = "alert alert-success";
                lblMensaje.Text = "Actualización exitosa. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }   
        }
    }
}