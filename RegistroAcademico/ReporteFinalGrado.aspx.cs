﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReportePeriodoGrado : System.Web.UI.Page
{
    String periodo;
    String grado;

    protected void Page_Load(object sender, EventArgs e)
    {
        grado = Request.Params["grado"];

        if (string.IsNullOrEmpty(grado))
        {
            Response.Redirect("DashboardReportes.aspx");
        }
    }
    
    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["anio"] = Convert.ToDecimal(DateTime.Now.Year);
        e.InputParameters["grado"] = grado;
    }
}