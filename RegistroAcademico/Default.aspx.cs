﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    Usuario Usuario = new Usuario();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UsuarioActivo"] != null)
        {
            Response.Redirect("~/DashboardProfesor.aspx");
        }
    }

    protected void btnIniciar_Click(object sender, EventArgs e)
    {
        Usuario.Id = txtUsuario.Text;
        Usuario.Contrasena = txtContra.Text;

        if (Usuario.Autenticar())
        {
            Session["UsuarioActivo"] = Usuario;
            Response.Redirect("~/DashboardProfesor.aspx");
        }
        else
        {
            lblMensaje.Text = "Datos incorrectos";
            panelSuccess.Visible = true;
        }
    }
}