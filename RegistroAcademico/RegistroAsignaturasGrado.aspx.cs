﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroAsignaturasGrado : System.Web.UI.Page
{
    Grado Grado = new Grado();
    Materia Materia = new Materia();
    Anio Anio = new Anio();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];

            try
            {
                //Años lectivos
                ddlAnio.DataSource = Anio.Anios();
                ddlAnio.DataBind();

                //Materias
                gdvAsignaturasGrado.DataSource = Materia.Listado();
                gdvAsignaturasGrado.DataBind();

                Grado.Id = Convert.ToInt32(id);

                if (id != null && Grado.Datos(DateTime.Today.Year))
                {
                    Session["gradoAsig"] = Grado;
                    
                    lblGrado.Text = Grado.Nombre;

                    //Historico de materias de grado
                    gdvAsignaturasHistorico.DataSource = Grado.HistoricoMateriasGrado();
                    gdvAsignaturasHistorico.DataBind();
                }
                else
                {
                    Session["gradoAsig"] = null;
                    lblMensaje.Text = "Grado no encontrado. ";
                    panelSuccess.CssClass = "alert alert-danger";
                    panelSuccess.Visible = true;
                }
            }
            catch (FormatException)
            {
                Session["gradoAsig"] = null;
                lblMensaje.Text = "Grado no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
        else
        {
            List<string> lista = new List<string>();
            foreach (GridViewRow item in gdvAsignaturasGrado.Rows)
            {
                if (((CheckBox)item.FindControl("chkRow")).Checked)
                    lista.Add(item.Cells[0].Text);
            }

            string msg = "";
            foreach (string item in lista)
                msg += item + "-";

            lblMensaje.Text = msg;
            panelSuccess.Visible = true;
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["gradoAsig"] != null)
        {
            Grado = (Grado)Session["gradoAsig"];
            Grado.Materias = new List<int>();

            foreach (GridViewRow fila in gdvAsignaturasGrado.Rows)
            {
                if (((CheckBox)fila.FindControl("chkRow")).Checked)
                    Grado.Materias.Add(Convert.ToInt32(fila.Cells[0].Text));
            }

            if (Grado.CrearMateriasGrado(Convert.ToInt32(ddlAnio.SelectedValue)) > 0)
            {
                //Recargar Historico
                gdvAsignaturasHistorico.DataSource = Grado.HistoricoMateriasGrado();
                gdvAsignaturasHistorico.DataBind();

                lblMensaje.Text = "Registro de currícula exitoso. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }
        }
    }
}