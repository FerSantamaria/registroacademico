﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroAdministrativo : System.Web.UI.Page
{
    Empleado Empleado = new Empleado();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Random rnd = new Random();

        string id = txtNombres.Text.Trim().Substring(0, 1) + txtPrimerApellido.Text.Trim().Substring(0, 1);
        string strRnd;

        do
        {
            strRnd = rnd.Next(999999).ToString().PadLeft(6, '0');
            Empleado.Id = id + strRnd;
        } while (Empleado.Datos());

        id += strRnd;

        Empleado.Id = id;
        Empleado.Nombres = txtNombres.Text;
        Empleado.Apellido1 = txtPrimerApellido.Text;
        Empleado.Apellido2 = txtSegundoApellido.Text;
        Empleado.Direccion = txtDireccion.Text;
        Empleado.Telefono = txtTelefono.Text;
        Empleado.Cargo = Convert.ToInt32(ddcargo.SelectedItem.Value);

        if (Empleado.Crear() > 1)
        {
            lblMensaje.Text = "Registro exitoso. Contraseña asignada: " + Empleado.Usuario.Contrasena + ". ";
            panelSuccess.Visible = true;
        }
        else
        {
            panelSuccess.CssClass = "alert alert-danger";
            lblMensaje.Text = "Ha ocurrido un error al guardar.";
            panelSuccess.Visible = true;
        }
    }
}