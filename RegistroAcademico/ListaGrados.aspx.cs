﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaGrados : System.Web.UI.Page
{
    Grado Grado = new Grado();

    protected void dgvBind()
    {
        DataSet ds = new DataSet();

        if (!IsPostBack || (IsPostBack && txtBusqueda.Text == ""))
            ds = Grado.Listado();
        else
            ds = Grado.Listado(txtBusqueda.Text);

        gdvGrados.DataSource = ds;
        gdvGrados.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        dgvBind();
    }
}