﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UsuarioActivo"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        else
        {
            Usuario usuario = (Usuario)Session["UsuarioActivo"];
            Empleado empleado = new Empleado();

            empleado.Id = usuario.Id;
            empleado.Datos();

            lblUsuario.Text = empleado.Nombres + " " + empleado.Apellido1;
        }
    }
}
