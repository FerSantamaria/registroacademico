﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaSecciones.aspx.cs" Inherits="ListaMaterias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Secciones</h2>

    <h3 class="sub-header">Nueva Sección</h3>
    <div class="col-md-12">
        <a class="btn btn-primary" href="RegistroSeccion.aspx">Registrar nueva sección</a>
    </div> 

    <div class="divide-10"></div>

    <h3 class="sub-header">Listado de secciones</h3>
    <div class="table-responsive" id="tblListaUsuarios">
        <asp:GridView ID="gdvSecciones" runat="server" AutoGenerateColumns="False" DataKeyNames="idSeccion" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="idSeccion"  HeaderText="ID"/>
                <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                <asp:TemplateField HeaderText="Opciones" ShowHeader="False">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-sm btn-info" NavigateUrl='<%# "EditarSeccion.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idSeccion") %>'>Editar</asp:HyperLink>
                        </ItemTemplate>
                    <ControlStyle CssClass="btn btn-info btn-sm" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Sin registros</EmptyDataTemplate>
        </asp:GridView>
    </div>

</asp:Content>

