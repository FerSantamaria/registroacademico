﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaEmpleados.aspx.cs" Inherits="ListaAdministrativo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Empleados</h2>
    
    <h3 class="sub-header">Nuevo empleado</h3>
    <div class="col-md-12">
        <a class="btn btn-primary" href="RegistroEmpleado.aspx">Registrar nuevo empleado</a>
    </div>

    <div class="divide-10"></div>

    <h3 class="sub-header">Búsqueda</h3>
    <div class="form-group col-xs-10 col-md-3">
        <asp:Label ID="Label1" runat="server" Text="ID o nombre" CssClass="sr-only" for="txtBusqueda"></asp:Label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-users"></i></div>
            <asp:TextBox ID="txtBusqueda" CssClass="form-control" runat="server" placeholder="ID o nombre"></asp:TextBox>
        </div>
    </div>
    <button type="submit" class="btn btn-primary col-xs-2 col-md-1">Buscar</button>

    <div class="divide-10"></div>

     <h3 class="sub-header">Listado de empleados</h3>
    <div class="table-responsive" id="tblListaAdministrativos">
        <asp:GridView ID="gdvEmpleado" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="idEmpleado" HeaderText="ID" />
                <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                <asp:BoundField DataField="Cargo"  HeaderText="Cargo"/>
                
                <asp:TemplateField HeaderText="Opciones" ShowHeader="False">
                    <ItemTemplate> 
                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-sm btn-info" NavigateUrl='<%# "EditarEmpleado.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idEmpleado") %>'>Editar</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-sm btn-danger" NavigateUrl='<%# "EliminarEmpleado.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idEmpleado") %>'>Eliminar</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Sin registros</EmptyDataTemplate>
        </asp:GridView>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

