﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroSeccion : System.Web.UI.Page
{
    Seccion Seccion = new Seccion();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Seccion.Nombre = txtNombre.Text.Substring(0, 1);

        if (Seccion.Crear() > 0)
        {
            lblMensaje.Text = "Registro exitoso. ";
            panelSuccess.Visible = true;
        }
        else
        {
            panelSuccess.CssClass = "alert alert-danger";
            lblMensaje.Text = "Ha ocurrido un error al guardar.";
            panelSuccess.Visible = true;
        }
    }
}