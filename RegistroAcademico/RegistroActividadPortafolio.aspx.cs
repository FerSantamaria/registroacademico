﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroActividad : System.Web.UI.Page
{
    public int grado;
    public int seccion;
    public int periodo;
    public int materia;
    Tareas Tareas = new Tareas();

    protected void Page_Load(object sender, EventArgs e)
    {
        lblPeriodo.Text = Tareas.getPeriodo(Convert.ToInt32(Session["periodo"]));
        lblMateria.Text = Tareas.getMateria(Convert.ToInt32(Session["materia"]));
        lblGrado.Text = Tareas.getGrado(Convert.ToInt32(Session["grado"]));
        lblSeccion.Text = Tareas.getSeccion(Convert.ToInt32(Session["seccion"]));
        if (IsPostBack)
        {
            this.grado = Convert.ToInt32(Session["grado"]);
            this.seccion = Convert.ToInt32(Session["seccion"]);
            this.periodo = Convert.ToInt32(Session["periodo"]);
            this.materia = Convert.ToInt32(Session["materia"]);            
            Session.Remove("grado");
            Session.Remove("seccion");
            Session.Remove("periodo");
            Session.Remove("materia");
        }
    }

    protected void btnRegistrar_Click(object sender, EventArgs e)
    {
        int queryResult;

        Tareas.idPeriodo = this.periodo;
        Tareas.idMateria = this.materia;
        Tareas.idGrado = this.grado;
        Tareas.idSeccion = this.seccion;
        Tareas.tarea = txtTitulo.Text;
        Tareas.descripcion = txtDescripcion.Text;
        Tareas.ponderacion = Convert.ToInt32(txtPorcentaje.Text);

        queryResult = Tareas.Crear();

        if (queryResult > 0)
        {
            Response.Redirect("~/ListaActividadesPortafolio.aspx?grado=" + this.grado + "&seccion=" + this.seccion + "&res=true");
        }
        else
        {
            panelSuccess.CssClass = "alert alert-danger";
            lblMensaje.Text = "Ha ocurrido un error al guardar.";
            panelSuccess.Visible = true;
        }
    }
}