﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EliminarAlumno : System.Web.UI.Page
{
    Alumnos Alumno = new Alumnos();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];
            Alumno.Id = id;

            if (id != null && Alumno.Datos())
            {
                lblCodigo.Text = Alumno.Id;
                lblNombre.Text = Alumno.Nombres + " " + Alumno.Apellido1 + " " + Alumno.Apellido2;
                lblNie.Text = Alumno.NIE;
                lblFecha.Text = Alumno.FechaIngreso.ToShortDateString();
                Session["eliAlum"] = Alumno;
            }
            else
            {
                Session["eliAlum"] = null;
                lblMensaje.Text = "Alumno no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        if (Session["eliAlum"] != null)
        {
            Alumno = (Alumnos)Session["eliAlum"];

            if (!Alumno.TieneRegistros())
            {
                if (Alumno.Eliminar() > 0)
                {
                    Session["eliAlum"] = null;
                    panelSuccess.CssClass = "alert alert-success";
                    lblMensaje.Text = "Alumno eliminado. ";
                    panelSuccess.Visible = true;
                }
                else
                {
                    panelSuccess.CssClass = "alert alert-danger";
                    lblMensaje.Text = "Ha ocurrido un error al eliminar.";
                    panelSuccess.Visible = true;
                }
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "No es posible eliminar el alumno porque posee otros registros.";
                panelSuccess.Visible = true;
            }
        }
    }
}