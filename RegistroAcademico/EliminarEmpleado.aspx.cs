﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EliminarEmpleado : System.Web.UI.Page
{
    Empleado Empleado = new Empleado();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];
            Empleado.Id = id;

            if (id != null && Empleado.Datos())
            {
                string cargo;

                switch (Empleado.Cargo)
                {
                    case 1:
                        cargo = "Profesor";
                        break;
                    case 2:
                        cargo = "Dirección";
                        break;
                    case 3:
                        cargo = "Secretaría";
                        break;
                    default:
                        cargo = "";
                        break;
                }

                lblCodigo.Text = Empleado.Id;
                lblNombre.Text = Empleado.Nombres + " " + Empleado.Apellido1 + " " + Empleado.Apellido2;
                lblCargo.Text = cargo;
                Session["eliEmp"] = Empleado;
            }
            else
            {
                Session["eliEmp"] = null;
                lblMensaje.Text = "Empleado no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        if (Session["eliEmp"] != null)
        {
            Empleado = (Empleado)Session["eliEmp"];

            if (!Empleado.TieneRegistros())
            {
                if (Empleado.Eliminar() > 0)
                {
                    Session["eliEmp"] = null;
                    panelSuccess.CssClass = "alert alert-success";
                    lblMensaje.Text = "Empleado eliminado. ";
                    panelSuccess.Visible = true;
                }
                else
                {
                    panelSuccess.CssClass = "alert alert-danger";
                    lblMensaje.Text = "Ha ocurrido un error al eliminar.";
                    panelSuccess.Visible = true;
                }
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "No es posible eliminar el empleado porque posee otros registros.";
                panelSuccess.Visible = true;
            }
        }
    }
}