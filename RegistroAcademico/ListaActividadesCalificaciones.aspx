﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaActividadesCalificaciones.aspx.cs" Inherits="ListaActividadesCalificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Portafolio</h2>
    
    <h3 class="sub-header">Registro de Calificaciones</h3>
    <div class="col-md-2">
        <label for="ddlPeriodo">Período:</label>
        <asp:DropDownList ID="ddlPeriodo" CssClass="form-control" runat="server">
            <asp:ListItem>01 2018</asp:ListItem>
            <asp:ListItem>02 2018</asp:ListItem>
            <asp:ListItem>03 2013</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="col-md-5">
        <label for="ddlPeriodo">Materia:</label>
        <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server">
            <asp:ListItem>Matemática</asp:ListItem>
            <asp:ListItem>Lenguaje</asp:ListItem>
        </asp:DropDownList>
    </div>
    
    <div class="clearfix"></div>
    <div class="divide-20"></div>

    <div class="col-md-12">
        <div class="table-responsive" id="tblListaUsuarios">
            <asp:GridView ID="gdvActividades" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" >
                <Columns>
                    <asp:BoundField DataField="Titulo"  HeaderText="Título"/>
                    <asp:BoundField DataField="Descripcion" ItemStyle-Width="50%" HeaderText="Descripción"/>
                    <asp:BoundField DataField="Porcentaje"  HeaderText="Porcentaje"/>
                    
                    <asp:TemplateField HeaderText="Opciones">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "RegistroCalificacionesActividad.aspx?id=" + DataBinder.Eval(Container.DataItem, "idTarea") %>'  CssClass="btn btn-sm btn-success" >Ingresar</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

