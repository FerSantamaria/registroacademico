﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="EditarProfesor.aspx.cs" Inherits="EditarProfesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2 class="header">Profesores</h2>
    <h3>Editar profesor</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaProfesor.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />

    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Título" ControlToValidate="txtTitulo" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Especialidad" ControlToValidate="txtEspecialidad" Display="None"></asp:RequiredFieldValidator>

    <div class="divide-10"></div>

    <div class="form-group col-sm-12">
        <h5><strong>Código:</strong> <asp:Label ID="lblCodigo" runat="server" Text=""></asp:Label></h5>
        <h5><strong>Nombre:</strong> <asp:Label ID="lblNombre" runat="server" Text=""></asp:Label></h5>
    </div>

    <div class="divide-10"></div>

    <h4 class="sub-header">Otra Información</h4>
    <div class="form-group col-sm-6">
        <label for="txtTitulo">Título:</label>
        <asp:TextBox ID="txtTitulo" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtEspecialidad">Especialidad:</label>
        <asp:TextBox ID="txtEspecialidad" CssClass="form-control" runat="server"></asp:TextBox>
    </div>

    <div class="divide-20"></div>

    <div class="col-md-12">
        <asp:Button ID="btnGuardar" CssClass="btn btn-success pull-right" runat="server" Text="Editar Profesor" OnClick="btnGuardar_Click" />
    </div>

    <div class="divide-20"></div>

</asp:Content>