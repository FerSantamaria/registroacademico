﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaActividadesCalificaciones : System.Web.UI.Page
{
    public int grado;
    public int seccion;

    Tareas Tareas = new Tareas();

    protected void Page_Load(object sender, EventArgs e)
    {
        //<-- DATOS PRUEBA DISEÑO -->
        DataTable datos = new DataTable();
        /*datos.Columns.Add("Titulo");
        datos.Columns.Add("Descripcion");
        datos.Columns.Add("Porcentaje");

        DataRow dr = datos.NewRow();
        dr["Titulo"] = "Tarea Ex-Aula";
        dr["Descripcion"] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris condimentum cursus neque, ac vulputate ante mattis non.";
        dr["Porcentaje"] = "10%";

        datos.Rows.Add(dr);*/

        this.grado = Convert.ToInt32(Request.QueryString["grado"]);
        this.seccion = Convert.ToInt32(Request.QueryString["seccion"]);

        datos = Tareas.ListadoTareasGrado(this.grado, this.seccion);
        
        gdvActividades.DataSource = datos;
        gdvActividades.DataBind();
        //<!-- DATOS PRUEBA DISEÑO -->
    }
}