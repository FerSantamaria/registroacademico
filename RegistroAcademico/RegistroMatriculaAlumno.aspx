﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="RegistroMatriculaAlumno.aspx.cs" Inherits="RegistroMatricula" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Alumnos</h2>
    <h3>Matricula</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="listaAlumnos.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />
    
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Grado" ControlToValidate="ddlGrado" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Sección" ControlToValidate="ddlSeccion" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Año Lectivo" ControlToValidate="ddlAnio" Display="None"></asp:RequiredFieldValidator>

    <div class="divide-10"></div>

    <div class="form-group col-sm-12">
        <h4><strong>Alumno: </strong><asp:Label ID="lblNombreAlumno" runat="server" Text=""></asp:Label></h4>
    </div>

    <div class="col-md-5">
        <h4 class="sub-header">Nueva Matricula</h4>
        <div class="form-group col-sm-6">
            <label for="ddlGrado">Grado:</label>
            <asp:DropDownList ID="ddlGrado" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGrado_SelectedIndexChanged">
                <%-- Grados --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-sm-6">
            <label for="ddlSeccion">Sección:</label>
            <asp:DropDownList ID="ddlSeccion" CssClass="form-control" runat="server">
                <%-- Secciones --%>
            </asp:DropDownList>
        </div>
        <div class="form-group col-sm-6">
            <label for="ddlAnio">Año Lectivo:</label>
            <asp:DropDownList ID="ddlAnio" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAnio_SelectedIndexChanged">
                <%-- Años lectivos --%>
            </asp:DropDownList>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-sm-12">
            <asp:CheckBox ID="chkRepite" runat="server" />
            <label for="chkRepite">Recursando nivel</label>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-sm-12">
            <asp:Button ID="btnGuardar" CssClass="btn btn-success" runat="server" Text="Registrar matrícula" OnClick="btnGuardar_Click" />
        </div>
    </div>

    <div class="col-md-7">
        <h4 class="sub-header">Historial de Matriculas</h4>
        <div class="table-responsive" id="tblListaMatriculas">
            <asp:GridView ID="gdvMatriculas" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" ShowHeaderWhenEmpty="true" GridLines="None" >
                <Columns>
                    <asp:BoundField DataField="anioLectivo"  HeaderText="Año"/>
                    <asp:BoundField DataField="grado"  HeaderText="Grado"/>        
                    <asp:BoundField DataField="seccion"  HeaderText="Sección"/>
                    <asp:TemplateField HeaderText="Recursando">
                        <ItemTemplate>
                            <%# Convert.ToInt32(Eval("repite")) == 1 ? "SI" : "NO" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="repite"  HeaderText="Recursando"/>--%>
                </Columns>
                <EmptyDataTemplate>Sin Registros.</EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
    <div class="divide-10"></div>

</asp:Content>