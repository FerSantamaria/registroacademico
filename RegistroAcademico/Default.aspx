﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-sv">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Registro Académico - Centro Escolar Católico Don Bosco</title>

    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/dashboard.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <img src="imagenes/logo.png" alt="Registro académico" class="img-responsive" width="50" style="margin-top: -19px" />
                </a>
                <span class="navbar-text">Registro Académico - CECDB</span>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="divide-80"></div>

                <asp:Panel ID="panelSuccess" CssClass="alert alert-danger alert-dismissable" Visible="false" runat="server">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error: </strong><asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                </asp:Panel>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12 clearfix">
                            <div class="divide-10">
                                <br />
                                <br />
                                <br />
                                <br />
                            </div>
                            <center>
                                <%--<img src="imagenes/logo.png" alt="Registro académico" class="img-responsive" width="100px" />--%>
                                <h3 class="text-center">
                                    <asp:Image ID="Image1" runat="server" Height="160px" ImageUrl="~/imagenes/logo.png" Width="171px" />
                                </h3>
                                <h3 class="text-center">Inicio de Sesión</h3>
                                <p class="text-center">&nbsp;</p>
                            </center>
                        </div>
                        <div class="col-md-12">
                            <form id="form2" runat="server" class="form-horizontal">
                                <div class="form-group">
                                    <label for="txtUsuario" class="col-md-3 col-xs-4 control-label">Usuario:</label>
                                    <div class="col-md-9 col-xs-8">
                                        <asp:TextBox ID="txtUsuario" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo obligatorio" ControlToValidate="txtUsuario" Display="Static"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtUsuario" class="col-md-3 col-xs-4 control-label">Contraseña:</label>
                                    <div class="col-md-9 col-xs-8">
                                        <asp:TextBox ID="txtContra" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo obligatorio" ControlToValidate="txtContra" Display="Static"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtUsuario" class="col-md-3 col-xs-4 control-label"></label>
                                    <div class="col-md-9 col-xs-8">
                                        <asp:Button ID="btnIniciar" CssClass="btn btn-primary pull-right" runat="server" Text="Iniciar Sesión" OnClick="btnIniciar_Click" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/Sidebar.js"></script>
</body>
</html>
