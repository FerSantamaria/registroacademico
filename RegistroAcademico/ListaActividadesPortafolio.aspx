﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaActividadesPortafolio.aspx.cs" Inherits="RegistroActividadesPortafolio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Portafolio</h2>
    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <h3 class="sub-header">Actividades registradas</h3>
    <div class="col-md-2">
        <label for="ddlPeriodo">Período:</label>
        <asp:DropDownList ID="ddlPeriodo" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlPeriodo_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
    </div>
    <div class="col-md-5">
        <label for="ddlPeriodo">Materia:</label>
        <asp:DropDownList ID="ddlMateria" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlMateria_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
    </div>
    <div class="col-md-5">
        <div class="divide-25"></div>
        <a class="btn btn-primary" href="RegistroActividadPortafolio.aspx">Registrar Nueva Actividad</a>
    </div> 

    <div class="clearfix"></div>
    <div class="divide-20"></div>

    <div class="col-md-12">
        <div class="table-responsive" id="tblListaUsuarios">
            <asp:GridView ID="gdvActividades" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" >
                <Columns>
                    <asp:BoundField DataField="Titulo"  HeaderText="Título"/>
                    <asp:BoundField DataField="Descripcion" ItemStyle-Width="50%" HeaderText="Descripción"/>
                    <asp:BoundField DataField="Porcentaje"  HeaderText="Porcentaje"/>
                    <asp:TemplateField HeaderText="Opciones">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-sm btn-info" >Editar</asp:HyperLink>
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-sm btn-danger">Eliminar</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="customJavascript" Runat="Server">
</asp:Content>

