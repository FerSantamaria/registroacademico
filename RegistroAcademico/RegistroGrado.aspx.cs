﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroGrado : System.Web.UI.Page
{
    Seccion Seccion = new Seccion();
    Grado Grado = new Grado();
    Anio Anio = new Anio();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            ddlAnio.DataSource = Anio.Anios();
            ddlAnio.DataBind();

            cblSecciones.DataSource = Seccion.Secciones();
            cblSecciones.DataTextField = "Text";
            cblSecciones.DataValueField = "Value";
            cblSecciones.DataBind();
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Grado.Nombre = txtNombre.Text;
        Grado.Secciones = cblSecciones.Items.Cast<ListItem>()
                            .Where(li => li.Selected)
                            .ToList();

        if (Grado.Crear(Convert.ToInt32(ddlAnio.SelectedValue)) > 0)
        {
            lblMensaje.Text = "Registro exitoso. ";
            panelSuccess.Visible = true;
        }
        else
        {
            panelSuccess.CssClass = "alert alert-danger";
            lblMensaje.Text = "Ha ocurrido un error al guardar.";
            panelSuccess.Visible = true;
        }
    }
}