﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaGrados.aspx.cs" Inherits="ListaGrados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Grados</h2>

    <h3 class="sub-header">Nuevo Grado</h3>
    <div class="col-md-12">
        <a class="btn btn-primary" href="RegistroGrado.aspx">Registrar nuevo grado</a>
    </div>

    <div class="divide-10"></div>

    <h3 class="sub-header">Búsqueda</h3>
    <div class="form-group col-xs-10 col-md-3">
        <asp:Label ID="Label1" runat="server" Text="Nombre o ID" CssClass="sr-only" for="txtBusqueda"></asp:Label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-users"></i></div>
            <asp:TextBox ID="txtBusqueda" CssClass="form-control" runat="server" placeholder="Nombre o ID"></asp:TextBox>
        </div>
    </div>
    <button type="submit" class="btn btn-primary col-xs-2 col-md-1">Buscar</button>


    <div class="divide-10"></div>

    <h3 class="sub-header">Listado de grados</h3>
    <div class="table-responsive" id="tblListaUsuarios">
        <asp:GridView ID="gdvGrados" runat="server" AutoGenerateColumns="False" DataKeyNames="idGrado" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true" >
            <Columns>
                <asp:BoundField DataField="idGrado"  HeaderText="ID"/>
                <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                <asp:TemplateField HeaderText="Opciones" ShowHeader="False">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-sm btn-info" NavigateUrl='<%# "EditarGrado.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idGrado") %>'>Editar</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-sm btn-success" NavigateUrl='<%# "RegistroAsignaturasGrado.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idGrado") %>'>Registrar Currícula</asp:HyperLink>                        
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Sin registros</EmptyDataTemplate>
        </asp:GridView>
    </div>

</asp:Content>

