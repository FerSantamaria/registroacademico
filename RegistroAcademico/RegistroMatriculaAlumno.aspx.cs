﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroMatricula : System.Web.UI.Page
{
    Alumnos Alumno = new Alumnos();
    Matricula Matricula = new Matricula();
    Grado Grado = new Grado();
    Anio Anio = new Anio();

    private void MatriculasAlumno()
    {
        Matricula.IdAlumno = Alumno.Id;
        gdvMatriculas.DataSource = Matricula.ListaMatriculasAlumno();
        gdvMatriculas.DataBind();
    }

    private void SeccionesGrado()
    {
        Grado.Id = Convert.ToInt32(ddlGrado.SelectedValue);
        Grado.SeccionesGrado(Convert.ToInt32(ddlAnio.SelectedValue));

        ddlSeccion.DataSource = Grado.Secciones;
        ddlSeccion.DataTextField = "Text";
        ddlSeccion.DataValueField = "Value";
        ddlSeccion.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            ddlAnio.DataSource = Anio.Anios();
            ddlAnio.DataBind();

            ddlGrado.DataSource = Grado.Grados();
            ddlGrado.DataTextField = "Text";
            ddlGrado.DataValueField = "Value";
            ddlGrado.DataBind();

            SeccionesGrado();

            Alumno.Id = Request.Params["Id"];

            if (Alumno.Id != null && Alumno.Datos())
            {
                lblNombreAlumno.Text = Alumno.Nombres + " " + Alumno.Apellido1 + " " + Alumno.Apellido2;
                MatriculasAlumno();
                Session["alumMat"] = Alumno;
            }
            else
            {
                Session["alumMat"] = null;
                lblMensaje.Text = "Alumno no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void ddlAnio_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeccionesGrado();
    }

    protected void ddlGrado_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeccionesGrado();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["alumMat"] != null)
        {
            Alumno = (Alumnos)Session["alumMat"];

            Matricula.IdAlumno = Alumno.Id;
            Matricula.AnioLectivo = Convert.ToInt32(ddlAnio.SelectedValue);

            if (!Matricula.Existe())
            {
                Matricula.IdGrado = Convert.ToInt32(ddlGrado.SelectedValue);
                Matricula.IdSeccion = Convert.ToInt32(ddlSeccion.SelectedValue);
                Matricula.Repite = chkRepite.Checked;

                if (Matricula.Crear() > 0)
                {
                    MatriculasAlumno();
                    panelSuccess.CssClass = "alert alert-success";
                    lblMensaje.Text = "Registro exitoso. ";
                    panelSuccess.Visible = true;
                }
                else
                {
                    panelSuccess.CssClass = "alert alert-danger";
                    lblMensaje.Text = "Ha ocurrido un error al guardar.";
                    panelSuccess.Visible = true;
                }
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ya existe una matricula para el alumno en el año lectivo seleccionado.";
                panelSuccess.Visible = true;
            }
        }
    }
}