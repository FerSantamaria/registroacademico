﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaProfesor.aspx.cs" Inherits="ListaProfesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Profesores</h2>

    <h3 class="sub-header">Nuevo Profesor</h3>
    <div class="col-md-12">
        <a class="btn btn-primary" href="RegistroProfesor.aspx">Registrar nuevo profesor</a>
    </div> 

    <div class="divide-10"></div>

    <h3 class="sub-header">Listado de Profesores</h3>
    <div class="table-responsive" id="tblListaUsuarios">
        <asp:GridView ID="gdvProfesores" runat="server" AutoGenerateColumns="False" CssClass="table table-striped" GridLines="None" >
            <Columns>
                <asp:BoundField DataField="idEmpleado"  HeaderText="ID"/>
                <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                <asp:TemplateField HeaderText="Registrado">
                    <ItemTemplate>
                        <%# (int)Eval("Registrado") == 1 ? "<span class='label label-success'><i class='fa fa-check'></i></span></td>" : "<span class='label label-danger'><i class='fa fa-times'></i></span></td>" %>

                        <%--<asp:Label ID="Label2" runat="server" Text="" CssClass="label label-info"><i class='fa <%# (int)DataBinder.Eval(Container.DataItem, "Registrado") == 1 ? "fa-check" : "fa-times" %>'></i></asp:Label>--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Opciones" ShowHeader="False">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-sm btn-info" NavigateUrl='<%# "EditarProfesor.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idEmpleado") %>'>Editar</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# "RegistroAsignaturasProfesor.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idEmpleado") %>' CssClass="btn btn-sm btn-success">Registro Materias</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>            

            </Columns>
            <EmptyDataTemplate>Sin registros</EmptyDataTemplate>
        </asp:GridView>
    </div>

</asp:Content>