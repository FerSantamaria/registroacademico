﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="ListaAsignaturas.aspx.cs" Inherits="ListaMaterias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Asignaturas</h2>

    <h3 class="sub-header">Nueva Asignatura</h3>
    <div class="col-md-12">
        <a class="btn btn-primary" href="RegistroAsignatura.aspx">Registrar nueva asignatura</a>
    </div> 

    <div class="divide-10"></div>

    <h3 class="sub-header">Búsqueda</h3>
    <div class="form-group col-xs-10 col-md-3">
        <asp:Label ID="Label1" runat="server" Text="Nombre o ID" CssClass="sr-only" for="txtBusqueda"></asp:Label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-users"></i></div>
            <asp:TextBox ID="txtBusqueda" CssClass="form-control" runat="server" placeholder="Nombre o ID"></asp:TextBox>
        </div>
    </div>
    <button type="submit" class="btn btn-primary col-xs-2 col-md-1">Buscar</button>


    <div class="divide-10"></div>

    <h3 class="sub-header">Listado de asignaturas</h3>
    <div class="table-responsive" id="tblListaUsuarios">
        <asp:GridView ID="gdvAsignaturas" runat="server" AutoGenerateColumns="False" DataKeyNames="idMateria" CssClass="table table-striped" GridLines="None" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="idMateria"  HeaderText="ID"/>
                <asp:BoundField DataField="Nombre"  HeaderText="Nombre"/>
                <asp:TemplateField HeaderText="Opciones" ShowHeader="False">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-sm btn-info" NavigateUrl='<%# "EditarAsignatura.aspx?Id=" + DataBinder.Eval(Container.DataItem, "idMateria") %>'>Editar</asp:HyperLink>
                        </ItemTemplate>
                    <ControlStyle CssClass="btn btn-info btn-sm" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Sin registros</EmptyDataTemplate>
        </asp:GridView>
    </div>

</asp:Content>

