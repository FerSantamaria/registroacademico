﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="RegistroSeccion.aspx.cs" Inherits="RegistroSeccion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Secciones</h2>
    <h3>Nueva sección</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaSecciones.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nombre" ControlToValidate="txtNombre" Display="None"></asp:RequiredFieldValidator>

    <div class="divide-10"></div>

    <h4 class="sub-header">Información de la Sección</h4>
    <div class="form-group col-sm-12">
        <label for="txtNombre">Nombre de la sección:</label>
        <asp:TextBox ID="txtNombre" CssClass="form-control" runat="server" MaxLength="1"></asp:TextBox>
    </div>

    <div class="divide-20"></div>

    <div class="col-md-12">
        <asp:Button ID="btnGuardar" CssClass="btn btn-success pull-right" runat="server" Text="Registrar Sección" OnClick="btnGuardar_Click" />
    </div>

    <div class="divide-20"></div>

</asp:Content>