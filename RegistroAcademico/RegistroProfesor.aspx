﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="RegistroProfesor.aspx.cs" Inherits="RegistroProfesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2 class="header">Profesores</h2>
    <h3>Nuevo profesor</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaProfesor.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />

    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Empleado a asignar" ControlToValidate="ddEmpleado" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Título" ControlToValidate="txtTitulo" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Especialidad" ControlToValidate="txtEspecialidad" Display="None"></asp:RequiredFieldValidator>

    <div class="divide-10"></div>

    <h4 class="sub-header">Empleado a asignar</h4>
    <div class="form-group col-sm-6">
        <label for="ddcargo">Empleado:</label>
        <asp:DropDownList ID="ddEmpleado" CssClass="form-control" runat="server" DataValueField="Value" DataTextField="Text">
        </asp:DropDownList>
    </div>

    <div class="divide-20"></div>

    <h4 class="sub-header">Otra Información</h4>
    <div class="form-group col-sm-6">
        <label for="txtTitulo">Título:</label>
        <asp:TextBox ID="txtTitulo" onkeypress="return letras(event);" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group col-sm-6">
        <label for="txtEspecialidad">Especialidad:</label>
        <asp:TextBox ID="txtEspecialidad" onkeypress="return letras(event);" CssClass="form-control" runat="server"></asp:TextBox>
    </div>

    <div class="divide-20"></div>

    <div class="col-md-12">
        <asp:Button ID="btnGuardar" CssClass="btn btn-success pull-right" runat="server" Text="Registrar Profesor" OnClick="btnGuardar_Click" />
    </div>

    <div class="divide-20"></div>

</asp:Content>