﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroActividadesPortafolio : System.Web.UI.Page
{
    Tareas Tareas = new Tareas();

    public int grado;
    public int seccion;

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 3)
            {
                Response.Redirect("~/Reportes.aspx");
            }
        }

        bool result = Convert.ToBoolean(Request.QueryString["res"]);

        /* --- ************************ --- */
        this.grado = Convert.ToInt32(Request.QueryString["grado"]);
        this.seccion = Convert.ToInt32(Request.QueryString["seccion"]);
        Session["grado"] = Request.QueryString["grado"];
        Session["seccion"] = Request.QueryString["seccion"];

        //<-- DATOS PRUEBA DISEÑO -->     
       
       if(!IsPostBack)
        {            
            //Binding de DDL Periodo
            ddlPeriodo.DataValueField = "idPeriodo";
            ddlPeriodo.DataTextField = "Periodo";
            ddlPeriodo.DataSource = Tareas.ListaPeriodo();
            ddlPeriodo.DataBind();
            Session["periodo"] = ddlPeriodo.SelectedItem.Value.ToString();

            //Binding de DDL Materia
            ddlMateria.DataValueField = "idMateria";
            ddlMateria.DataTextField = "Nombre";
            ddlMateria.DataSource = Tareas.ListaMaterias();
            ddlMateria.DataBind();
            Session["materia"] = ddlMateria.SelectedItem.Value.ToString();
        }

       if (result)
        {
            lblMensaje.Text = "Registro exitoso!";
            panelSuccess.Visible = true;
        }

        DataTable datos = new DataTable();
        /*datos.Columns.Add("Titulo");
        datos.Columns.Add("Descripcion");
        datos.Columns.Add("Porcentaje");

        DataRow dr = datos.NewRow();
        dr["Titulo"] = "Tarea Ex-Aula";
        dr["Descripcion"] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris condimentum cursus neque, ac vulputate ante mattis non.";
        dr["Porcentaje"] = "10%";

        datos.Rows.Add(dr);*/
        datos = Tareas.ListadoTareasGrado(this.grado, this.seccion);

        gdvActividades.DataSource = datos;
        gdvActividades.DataBind();
        //<!-- DATOS PRUEBA DISEÑO -->
    }

    protected void ddlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["periodo"] = ddlPeriodo.SelectedItem.Value.ToString();
        ddlPeriodo.SelectedValue = ddlPeriodo.SelectedItem.Value.ToString();
    }

    protected void ddlMateria_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["materia"] = ddlMateria.SelectedItem.Value.ToString();
        ddlMateria.SelectedValue = ddlMateria.SelectedItem.Value.ToString();
    }
}