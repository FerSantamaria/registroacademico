﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ListaAdministrativo : System.Web.UI.Page
{
    Empleado Empleado = new Empleado();

    protected void dgvBind()
    {
        DataSet ds = new DataSet();

        if (!IsPostBack || (IsPostBack && txtBusqueda.Text == ""))
            ds = Empleado.Listado();
        else
            ds = Empleado.Listado(txtBusqueda.Text);

        gdvEmpleado.DataSource = ds;
        gdvEmpleado.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        dgvBind();
    }
}