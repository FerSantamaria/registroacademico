﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroMateriasProfesor : System.Web.UI.Page
{
    Grado Grado = new Grado();
    Anio Anio = new Anio();
    Profesor Profesor = new Profesor();

    private void MateriasProfesor()
    {
        gdvAsignaturasProfesor.DataSource = Profesor.MateriasProfesor();
        gdvAsignaturasProfesor.DataBind();
    }

    private void MateriasGrado()
    {
        Grado.Id = Convert.ToInt32(ddlGrado.SelectedValue);
        gdvAsignaturasGrado.DataSource = Grado.MateriasGrado(Convert.ToInt32(ddlAnio.SelectedValue));
        gdvAsignaturasGrado.DataBind();
    }

    private void SeccionesGrado()
    {
        Grado.Id = Convert.ToInt32(ddlGrado.SelectedValue);
        Grado.SeccionesGrado(Convert.ToInt32(ddlAnio.SelectedValue));

        ddlSeccion.DataSource = Grado.Secciones;
        ddlSeccion.DataTextField = "Text";
        ddlSeccion.DataValueField = "Value";
        ddlSeccion.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];
            Profesor.Id = id;

            //Años lectivos
            ddlAnio.DataSource = Anio.Anios();
            ddlAnio.DataBind();

            //Grados
            ddlGrado.DataSource = Grado.Grados();
            ddlGrado.DataTextField = "Text";
            ddlGrado.DataValueField = "Value";
            ddlGrado.DataBind();

            //Secciones
            SeccionesGrado();

            //Materias
            MateriasGrado();

            if (id != null && Profesor.Datos())
            {
                lblProfesor.Text = Profesor.Nombres + " " + Profesor.Apellido1 + " " + Profesor.Apellido2;
                MateriasProfesor();
                Session["profAsig"] = Profesor;
            }
            else
            {
                Session["profAsig"] = null;
                lblMensaje.Text = "Profesor no encontrado/registrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["profAsig"] != null)
        {
            Profesor = (Profesor)Session["profAsig"];
            List<int> materias = new List<int>();

            foreach (GridViewRow fila in gdvAsignaturasGrado.Rows)
            {
                if (((CheckBox)fila.FindControl("chkRow")).Checked)
                    materias.Add(Convert.ToInt32(fila.Cells[0].Text));
            }

            int anio = Convert.ToInt32(ddlAnio.SelectedValue);
            int grado = Convert.ToInt32(ddlGrado.SelectedValue);
            int seccion = Convert.ToInt32(ddlSeccion.SelectedValue);

            if (Profesor.CrearMateriasProfesor(anio, grado, seccion, materias) > 0)
            {
                //Recargar Historico
                MateriasProfesor();

                panelSuccess.CssClass = "alert alert-success";
                lblMensaje.Text = "Registro de currícula exitoso. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void ddlAnio_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeccionesGrado();
        MateriasGrado();
    }

    protected void ddlGrado_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeccionesGrado();
        MateriasGrado();
    }
}