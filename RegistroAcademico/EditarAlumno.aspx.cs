﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditarAlumno : System.Web.UI.Page
{
    Alumnos Alumno = new Alumnos();
    Departamentos dep = new Departamentos();

    private void Muns()
    {
        int idDep = Convert.ToInt32(ddlDepartamento.SelectedValue);
        ddlMunicipio.DataSource = dep.municipios(idDep);
        ddlMunicipio.DataValueField = "ID";
        ddlMunicipio.DataTextField = "MunName";
        ddlMunicipio.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack){
            ddlDepartamento.DataSource = dep.departamentos();
            ddlDepartamento.DataValueField = "ID";
            ddlDepartamento.DataTextField = "DepName";
            ddlDepartamento.DataBind();
            
            string id = Request.Params["Id"];
            Alumno.Id = id;

            if (id != null && Alumno.Datos())
            {
                txtNIE.Text = Alumno.NIE;
                txtNombres.Text = Alumno.Nombres;
                txtPrimerApellido.Text = Alumno.Apellido1;
                txtSegundoApellido.Text = Alumno.Apellido2;
                txtDireccion.Text = Alumno.Direccion;
                calNacimiento.Text = Alumno.FechaNacimiento.ToString("yyyy-MM-dd");
                calIngreso.Text = Alumno.FechaIngreso.ToString("yyyy-MM-dd");
                ddlDepartamento.SelectedValue = Alumno.Departamento.ToString();
                Muns();
                ddlMunicipio.SelectedValue = Alumno.Municipio.ToString();
                ddlSexo.SelectedValue = Alumno.Sexo.ToString();
                txtPartida.Text = Alumno.Partida;
                txtFolio.Text = Alumno.Folio.ToString();

                //Familia
                txtVive.Text = Alumno.Habitantes;
                txtResponsable.Text = Alumno.Responsable;
                txtDUI.Text = Alumno.ResponsableDUI;
                txtProfResp.Text = Alumno.ResponsableProfesion;
                txtEstCivil.SelectedValue = Alumno.EstadoCivilPadres.ToString();
                txtMiembros.Text = Alumno.MiembrosFamilia.ToString();
                txtNoLeen.Text = Alumno.NoEscribenLeen.ToString();

                //Info Emergencia
                txtContEmergencia.Text = Alumno.ContactoEmergencia;
                txtParentesco.Text = Alumno.ParentescoEmergencia;
                txtTelefono.Text = Alumno.TelefonoEmergencia;
                txtAlergias.Text = string.Join(",", Alumno.Salud.ToArray());

                //Religion
                txtReligion.Text = Alumno.Religion;
                CheckBoxList1.Items[0].Selected = Alumno.Bautismo == 1 ? true : false;
                CheckBoxList1.Items[1].Selected = Alumno.Comunion == 1 ? true : false;
                CheckBoxList1.Items[2].Selected = Alumno.Confirmacion == 1 ? true : false;

                Session["editAlum"] = Alumno;
            }
            else
            {
                Session["editAlum"] = null;
                lblMensaje.Text = "Alumno no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["editAlum"] != null)
        {
            Alumno = (Alumnos)Session["editAlum"];

            Alumno.NIE = txtNIE.Text;
            Alumno.Nombres = txtNombres.Text;
            Alumno.Apellido1 = txtPrimerApellido.Text;
            Alumno.Apellido2 = txtSegundoApellido.Text;
            Alumno.Direccion = txtDireccion.Text;
            Alumno.FechaNacimiento = DateTime.Parse(calNacimiento.Text);
            Alumno.FechaIngreso = DateTime.Parse(calIngreso.Text);
            Alumno.Municipio = Convert.ToInt32(ddlMunicipio.SelectedValue);
            Alumno.Sexo = Convert.ToInt32(ddlSexo.SelectedValue);
            Alumno.Partida = txtPartida.Text;
            Alumno.Folio = Convert.ToInt32(txtFolio.Text);

            //Familia
            Alumno.Habitantes = txtVive.Text;
            Alumno.Responsable = txtResponsable.Text;
            Alumno.ResponsableDUI = txtDUI.Text;
            Alumno.ResponsableProfesion = txtProfResp.Text;
            Alumno.EstadoCivilPadres = Convert.ToInt32(txtEstCivil.SelectedValue);
            Alumno.MiembrosFamilia = Convert.ToInt32(txtMiembros.Text);
            Alumno.NoEscribenLeen = Convert.ToInt32(txtNoLeen.Text);

            //Info Emergencia
            Alumno.ContactoEmergencia = txtContEmergencia.Text;
            Alumno.ParentescoEmergencia = txtParentesco.Text;
            Alumno.TelefonoEmergencia = txtTelefono.Text;
            Alumno.Salud = txtAlergias.Text.Split(',').OfType<string>().ToList();

            //Religion
            Alumno.Religion = txtReligion.Text;
            Alumno.Bautismo = CheckBoxList1.Items[0].Selected ? 1 : 0;
            Alumno.Comunion = CheckBoxList1.Items[1].Selected ? 1 : 0;
            Alumno.Confirmacion = CheckBoxList1.Items[2].Selected ? 1 : 0;

            if (Alumno.Actualizar() > 0)
            {
                panelSuccess.CssClass = "alert alert-success";
                lblMensaje.Text = "Actualización exitosa. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Muns();
    }
}