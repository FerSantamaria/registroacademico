﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListaProfesor : System.Web.UI.Page
{
    Profesor Profesor = new Profesor();

    protected void dgvBind()
    {
        DataSet ds = new DataSet();
        ds = Profesor.Listado();

        gdvProfesores.DataSource = ds;
        gdvProfesores.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1 || usuario.Tipo == 3)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        dgvBind();
    }
}