﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditarGrado : System.Web.UI.Page
{
    Seccion Seccion = new Seccion();

    protected void Page_Load(object sender, EventArgs e)
    {
        /* --- Validaciones de sesiones --- */
        Usuario usuario = (Usuario)Session["UsuarioActivo"];

        if (usuario != null)
        {
            if (usuario.Tipo == 1)
            {
                Response.Redirect("~/DashboardProfesor.aspx");
            }
        }
        /* --- ************************ --- */

        if (!IsPostBack)
        {
            string id = Request.Params["Id"];

            try
            {
                Seccion.Id = Convert.ToInt32(id);

                if (id != null && Seccion.Datos())
                {
                    txtNombre.Text = Seccion.Nombre;
                    Session["editSec"] = Seccion;
                }
                else
                {
                    Session["editSec"] = null;
                    lblMensaje.Text = "Asignatura no encontrado. ";
                    panelSuccess.CssClass = "alert alert-danger";
                    panelSuccess.Visible = true;
                }
            }
            catch (FormatException)
            {
                Session["editSec"] = null;
                lblMensaje.Text = "Asignatura no encontrado. ";
                panelSuccess.CssClass = "alert alert-danger";
                panelSuccess.Visible = true;
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Session["editSec"] != null)
        {
            Seccion = (Seccion)Session["editSec"];

            Seccion.Nombre = txtNombre.Text;

            if (Seccion.Actualizar() > 0)
            {
                lblMensaje.Text = "Actualización exitosa. ";
                panelSuccess.Visible = true;
            }
            else
            {
                panelSuccess.CssClass = "alert alert-danger";
                lblMensaje.Text = "Ha ocurrido un error al guardar.";
                panelSuccess.Visible = true;
            }
        }
    }
}