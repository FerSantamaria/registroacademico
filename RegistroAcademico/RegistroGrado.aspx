﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeFile="RegistroGrado.aspx.cs" Inherits="RegistroGrado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h2 class="header">Grados</h2>
    <h3>Nuevo grado</h3>

    <asp:Panel ID="panelSuccess" CssClass="alert alert-success" Visible="false" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        Volver a la <a href="ListaGrados.aspx">lista</a>
    </asp:Panel>

    <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" CssClass="alert alert-danger alert-dismissable" HeaderText="Campos obligatorios: " runat="server" />

    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nombre" ControlToValidate="txtNombre" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Año Lectivo" ControlToValidate="ddlAnio" Display="None"></asp:RequiredFieldValidator>
    <asp:CustomValidator runat="server" ID="CustomFieldValidator1" ClientValidationFunction="ValidateModuleList" ErrorMessage="Secciones (al menos una)" Display="None" ></asp:CustomValidator>

    <div class="divide-10"></div>

    <div class="col-md-5">
        <h4 class="sub-header">Información del Grado</h4>
        <div class="form-group col-sm-12">
            <label for="txtNombre">Nombre del grado:</label>
            <asp:TextBox ID="txtNombre" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-sm-12">
            <label for="ddlAnio">Año Lectivo:</label>
            <asp:DropDownList ID="ddlAnio" CssClass="form-control" runat="server">
                <%-- Años lectivos --%>
            </asp:DropDownList>
        </div>
    </div>
    
    <div class="col-md-7">
        <h4 class="sub-header">Secciones</h4>
        <div class="form-group col-sm-12">
            <label>Secciones:</label>
            <asp:CheckBoxList CssClass="checkboxlistformat" ID="cblSecciones" runat="server">

            </asp:CheckBoxList>
        </div>
    </div>

    <div class="divide-20"></div>

    <div class="col-md-12">
        <asp:Button ID="btnGuardar" CssClass="btn btn-success pull-right" runat="server" Text="Registrar Grado" OnClick="btnGuardar_Click" />
    </div>

    <div class="divide-20"></div>

    <script type="text/javascript">
        function ValidateModuleList(source, args)
        {
            var chkListModules= document.getElementById ('<%= cblSecciones.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i=0;i<chkListinputs .length;i++)
            {
                if (chkListinputs [i].checked)
                {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>