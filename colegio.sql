USE [master]
GO
/****** Object:  Database [colegio]    Script Date: 11/9/2018 10:45:04 PM ******/
CREATE DATABASE [colegio]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'colegio', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\colegio.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'colegio_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\colegio_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [colegio] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [colegio].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [colegio] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [colegio] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [colegio] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [colegio] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [colegio] SET ARITHABORT OFF 
GO
ALTER DATABASE [colegio] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [colegio] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [colegio] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [colegio] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [colegio] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [colegio] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [colegio] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [colegio] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [colegio] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [colegio] SET  ENABLE_BROKER 
GO
ALTER DATABASE [colegio] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [colegio] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [colegio] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [colegio] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [colegio] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [colegio] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [colegio] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [colegio] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [colegio] SET  MULTI_USER 
GO
ALTER DATABASE [colegio] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [colegio] SET DB_CHAINING OFF 
GO
ALTER DATABASE [colegio] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [colegio] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [colegio] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [colegio] SET QUERY_STORE = OFF
GO
USE [colegio]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [colegio]
GO
/****** Object:  Table [dbo].[Alumno]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alumno](
	[idAlumno] [varchar](8) NOT NULL,
	[NIE] [varchar](7) NOT NULL,
	[nombres] [varchar](50) NOT NULL,
	[apellido1] [varchar](25) NOT NULL,
	[apellido2] [varchar](25) NULL,
	[municipio] [int] NOT NULL,
	[partida] [varchar](max) NOT NULL,
	[folio] [int] NULL,
	[sexo] [tinyint] NOT NULL,
	[direccion] [text] NOT NULL,
	[habitantes] [text] NOT NULL,
	[responsablePersona] [varchar](100) NOT NULL,
	[responsableDui] [varchar](10) NOT NULL,
	[responsableProfesion] [varchar](50) NULL,
	[miembrosFamilia] [int] NOT NULL,
	[familiarLecturaEscritura] [int] NOT NULL,
	[estadoCivil] [int] NOT NULL,
	[fechaNacimiento] [date] NOT NULL,
	[fechaIngreso] [date] NOT NULL,
	[emergenciaContact] [varchar](100) NULL,
	[emergenciaParentesco] [varchar](25) NULL,
	[telefono] [varchar](8) NOT NULL,
	[religion] [varchar](50) NULL,
	[bautismo] [tinyint] NULL,
	[confirmacion] [tinyint] NULL,
	[primeraComunion] [tinyint] NULL,
	[fechaRegistrado] [datetime] NULL,
 CONSTRAINT [PK_Alumno] PRIMARY KEY CLUSTERED 
(
	[idAlumno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Anio]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Anio](
	[Anio] [int] NOT NULL,
UNIQUE NONCLUSTERED 
(
	[Anio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DEPSV]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DEPSV](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DepName] [nvarchar](30) NOT NULL,
	[ISOCode] [nchar](5) NOT NULL,
	[ZONESV_ID] [int] NOT NULL,
 CONSTRAINT [PK_DEPSV] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleGradoMateria]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleGradoMateria](
	[idGrado] [int] NOT NULL,
	[idMateria] [int] NOT NULL,
	[anioLectivo] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleGradoSeccion]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleGradoSeccion](
	[idSeccion] [int] NOT NULL,
	[idGrado] [int] NOT NULL,
	[anioLectivo] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleMateriaProfesor]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleMateriaProfesor](
	[idProfesor] [varchar](8) NOT NULL,
	[idMateria] [int] NOT NULL,
	[idGrado] [int] NOT NULL,
	[idSeccion] [int] NOT NULL,
	[anioLectivo] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetallePeriodo]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetallePeriodo](
	[idPeriodo] [int] NOT NULL,
	[fechaInicio] [date] NOT NULL,
	[fechaFin] [date] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalleTarea]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalleTarea](
	[idTarea] [int] IDENTITY(1,1) NOT NULL,
	[idMateria] [int] NOT NULL,
	[idPeriodo] [int] NOT NULL,
	[tarea] [varchar](50) NOT NULL,
	[descripcion] [text] NULL,
	[ponderacion] [int] NOT NULL,
	[idGrado] [int] NULL,
	[idSeccion] [int] NULL,
 CONSTRAINT [PK_detalleTarea] PRIMARY KEY CLUSTERED 
(
	[idTarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[idEmpleado] [varchar](8) NOT NULL,
	[nombres] [varchar](25) NOT NULL,
	[Apellido1] [varchar](25) NOT NULL,
	[Apellido2] [varchar](25) NULL,
	[Direccion] [varchar](100) NOT NULL,
	[Telefono] [varchar](8) NOT NULL,
	[cargo] [int] NOT NULL,
 CONSTRAINT [PK_Administrativo] PRIMARY KEY CLUSTERED 
(
	[idEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Grado]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grado](
	[idGrado] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](25) NOT NULL,
 CONSTRAINT [PK_Grado] PRIMARY KEY CLUSTERED 
(
	[idGrado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Materia]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Materia](
	[idMateria] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Materia] PRIMARY KEY CLUSTERED 
(
	[idMateria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Matricula]    Script Date: 11/9/2018 10:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Matricula](
	[idMatricula] [int] IDENTITY(1,1) NOT NULL,
	[idAlumno] [varchar](8) NOT NULL,
	[idGrado] [int] NOT NULL,
	[idSeccion] [int] NOT NULL,
	[anioLectivo] [int] NOT NULL,
	[repite] [tinyint] NOT NULL,
 CONSTRAINT [PK_Matricula] PRIMARY KEY CLUSTERED 
(
	[idMatricula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MUNSV]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MUNSV](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MunName] [nvarchar](100) NOT NULL,
	[DEPSV_ID] [int] NOT NULL,
 CONSTRAINT [PK_MUNSV] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[notaTarea]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notaTarea](
	[idTarea] [int] NOT NULL,
	[idMatricula] [int] NOT NULL,
	[nota] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Periodo]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Periodo](
	[idPeriodo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](25) NOT NULL,
 CONSTRAINT [PK_Periodo] PRIMARY KEY CLUSTERED 
(
	[idPeriodo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profesor]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profesor](
	[idProfesor] [varchar](8) NOT NULL,
	[titulo] [varchar](50) NOT NULL,
	[especialidad] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Profesor] PRIMARY KEY CLUSTERED 
(
	[idProfesor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salud]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salud](
	[enfermedad] [varchar](50) NULL,
	[idAlumno] [varchar](8) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Seccion]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seccion](
	[idSeccion] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](1) NOT NULL,
 CONSTRAINT [PK_Seccion] PRIMARY KEY CLUSTERED 
(
	[idSeccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[idPersona] [varchar](8) NOT NULL,
	[contraseña] [varchar](255) NOT NULL,
	[tipo] [tinyint] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZONESV]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZONESV](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ZoneName] [varchar](15) NOT NULL,
 CONSTRAINT [PK_ZONESV] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Alumno] ADD  CONSTRAINT [DF__Alumno__fechaReg__2D27B809]  DEFAULT (getdate()) FOR [fechaRegistrado]
GO
ALTER TABLE [dbo].[Alumno]  WITH CHECK ADD  CONSTRAINT [FK_Alumno_MUNSV] FOREIGN KEY([municipio])
REFERENCES [dbo].[MUNSV] ([ID])
GO
ALTER TABLE [dbo].[Alumno] CHECK CONSTRAINT [FK_Alumno_MUNSV]
GO
ALTER TABLE [dbo].[DEPSV]  WITH CHECK ADD  CONSTRAINT [FK_DEPSV_ZONESV] FOREIGN KEY([ZONESV_ID])
REFERENCES [dbo].[ZONESV] ([ID])
GO
ALTER TABLE [dbo].[DEPSV] CHECK CONSTRAINT [FK_DEPSV_ZONESV]
GO
ALTER TABLE [dbo].[DetalleGradoMateria]  WITH CHECK ADD  CONSTRAINT [FK_GradoMateria_Grado] FOREIGN KEY([idGrado])
REFERENCES [dbo].[Grado] ([idGrado])
GO
ALTER TABLE [dbo].[DetalleGradoMateria] CHECK CONSTRAINT [FK_GradoMateria_Grado]
GO
ALTER TABLE [dbo].[DetalleGradoMateria]  WITH CHECK ADD  CONSTRAINT [FK_GradoMateria_Materia] FOREIGN KEY([idMateria])
REFERENCES [dbo].[Materia] ([idMateria])
GO
ALTER TABLE [dbo].[DetalleGradoMateria] CHECK CONSTRAINT [FK_GradoMateria_Materia]
GO
ALTER TABLE [dbo].[DetalleGradoSeccion]  WITH CHECK ADD  CONSTRAINT [FK_GradoSeccion_Grado] FOREIGN KEY([idGrado])
REFERENCES [dbo].[Grado] ([idGrado])
GO
ALTER TABLE [dbo].[DetalleGradoSeccion] CHECK CONSTRAINT [FK_GradoSeccion_Grado]
GO
ALTER TABLE [dbo].[DetalleGradoSeccion]  WITH CHECK ADD  CONSTRAINT [FK_GradoSeccion_Seccion] FOREIGN KEY([idSeccion])
REFERENCES [dbo].[Seccion] ([idSeccion])
GO
ALTER TABLE [dbo].[DetalleGradoSeccion] CHECK CONSTRAINT [FK_GradoSeccion_Seccion]
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor]  WITH CHECK ADD  CONSTRAINT [FK_DetalleMateriaProfesor_Seccion] FOREIGN KEY([idSeccion])
REFERENCES [dbo].[Seccion] ([idSeccion])
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor] CHECK CONSTRAINT [FK_DetalleMateriaProfesor_Seccion]
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor]  WITH CHECK ADD  CONSTRAINT [FK_MateriaProfesor_Grado] FOREIGN KEY([idGrado])
REFERENCES [dbo].[Grado] ([idGrado])
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor] CHECK CONSTRAINT [FK_MateriaProfesor_Grado]
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor]  WITH CHECK ADD  CONSTRAINT [FK_MateriaProfesor_Materia] FOREIGN KEY([idMateria])
REFERENCES [dbo].[Materia] ([idMateria])
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor] CHECK CONSTRAINT [FK_MateriaProfesor_Materia]
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor]  WITH CHECK ADD  CONSTRAINT [FK_MateriaProfesor_Profesor] FOREIGN KEY([idProfesor])
REFERENCES [dbo].[Profesor] ([idProfesor])
GO
ALTER TABLE [dbo].[DetalleMateriaProfesor] CHECK CONSTRAINT [FK_MateriaProfesor_Profesor]
GO
ALTER TABLE [dbo].[DetallePeriodo]  WITH CHECK ADD  CONSTRAINT [FK_DetallePeriodo_Periodo] FOREIGN KEY([idPeriodo])
REFERENCES [dbo].[Periodo] ([idPeriodo])
GO
ALTER TABLE [dbo].[DetallePeriodo] CHECK CONSTRAINT [FK_DetallePeriodo_Periodo]
GO
ALTER TABLE [dbo].[detalleTarea]  WITH CHECK ADD  CONSTRAINT [FK_detalleTarea_Materia] FOREIGN KEY([idMateria])
REFERENCES [dbo].[Materia] ([idMateria])
GO
ALTER TABLE [dbo].[detalleTarea] CHECK CONSTRAINT [FK_detalleTarea_Materia]
GO
ALTER TABLE [dbo].[detalleTarea]  WITH CHECK ADD  CONSTRAINT [FK_detalleTarea_Periodo] FOREIGN KEY([idPeriodo])
REFERENCES [dbo].[Periodo] ([idPeriodo])
GO
ALTER TABLE [dbo].[detalleTarea] CHECK CONSTRAINT [FK_detalleTarea_Periodo]
GO
ALTER TABLE [dbo].[Matricula]  WITH CHECK ADD  CONSTRAINT [FK_Matricula_Alumno] FOREIGN KEY([idAlumno])
REFERENCES [dbo].[Alumno] ([idAlumno])
GO
ALTER TABLE [dbo].[Matricula] CHECK CONSTRAINT [FK_Matricula_Alumno]
GO
ALTER TABLE [dbo].[Matricula]  WITH CHECK ADD  CONSTRAINT [FK_Matricula_Grado] FOREIGN KEY([idGrado])
REFERENCES [dbo].[Grado] ([idGrado])
GO
ALTER TABLE [dbo].[Matricula] CHECK CONSTRAINT [FK_Matricula_Grado]
GO
ALTER TABLE [dbo].[Matricula]  WITH CHECK ADD  CONSTRAINT [FK_Matricula_Seccion] FOREIGN KEY([idSeccion])
REFERENCES [dbo].[Seccion] ([idSeccion])
GO
ALTER TABLE [dbo].[Matricula] CHECK CONSTRAINT [FK_Matricula_Seccion]
GO
ALTER TABLE [dbo].[MUNSV]  WITH CHECK ADD  CONSTRAINT [FK_MUNSV_DEPSV] FOREIGN KEY([DEPSV_ID])
REFERENCES [dbo].[DEPSV] ([ID])
GO
ALTER TABLE [dbo].[MUNSV] CHECK CONSTRAINT [FK_MUNSV_DEPSV]
GO
ALTER TABLE [dbo].[notaTarea]  WITH CHECK ADD  CONSTRAINT [FK_notaTarea_detalleTarea] FOREIGN KEY([idTarea])
REFERENCES [dbo].[detalleTarea] ([idTarea])
GO
ALTER TABLE [dbo].[notaTarea] CHECK CONSTRAINT [FK_notaTarea_detalleTarea]
GO
ALTER TABLE [dbo].[notaTarea]  WITH CHECK ADD  CONSTRAINT [FK_notaTarea_Matricula] FOREIGN KEY([idMatricula])
REFERENCES [dbo].[Matricula] ([idMatricula])
GO
ALTER TABLE [dbo].[notaTarea] CHECK CONSTRAINT [FK_notaTarea_Matricula]
GO
ALTER TABLE [dbo].[Profesor]  WITH CHECK ADD  CONSTRAINT [FK_Profesor_Empleado] FOREIGN KEY([idProfesor])
REFERENCES [dbo].[Empleado] ([idEmpleado])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Profesor] CHECK CONSTRAINT [FK_Profesor_Empleado]
GO
ALTER TABLE [dbo].[Salud]  WITH CHECK ADD  CONSTRAINT [FK_Salud_Alumno] FOREIGN KEY([idAlumno])
REFERENCES [dbo].[Alumno] ([idAlumno])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Salud] CHECK CONSTRAINT [FK_Salud_Alumno]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Empleado] FOREIGN KEY([idPersona])
REFERENCES [dbo].[Empleado] ([idEmpleado])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Empleado]
GO
/****** Object:  StoredProcedure [dbo].[nuevoAlumno]    Script Date: 11/9/2018 10:45:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nuevoAlumno] 
@nie varchar(7),
@nombres varchar(50),
@apellido1 varchar(25),
@apellido2 varchar(25),
@departamento int,
@municipio int,
@partida varchar(MAX),
@folio int, 
@sexo tinyint,
@direccion text,
@habitantes text,
@responsablePersona varchar(100),
@responsableDui varchar(10),
@responsableProfesion varchar(50),
@miembrosFamilia int,
@familiaLecturaEscritura int,
@estadoCivil int,
@telefono varchar(8),
@fechaNacimiento date,
@fechaIngreso date,
@emergenciaContacto varchar(100),
@emergenciaParentesco varchar(25),
@religion varchar(50),
@bautismo binary(1),
@confirmacion binary(1),
@primeraComunion binary(1)
AS
	DECLARE @id varchar(8)
	SET @id = (select SUBSTRING(@nombres,1,1) + SUBSTRING(@apellido1,1,1) + SUBSTRING((cast((convert(numeric(12,0),rand() * 899999999999) + 100000000000) as varchar(12))), 1, 6));
	
	INSERT INTO Alumno values (@id, @nie, @nombres, @apellido1, @apellido2, @municipio, @partida, @folio, @sexo, @direccion, @habitantes, @responsablePersona,
	@responsableDui, @responsableProfesion, @miembrosFamilia, @familiaLecturaEscritura, @estadoCivil, @fechaNacimiento, @fechaIngreso, @emergenciaContacto, @emergenciaParentesco, @telefono, 
	@religion, @bautismo, @confirmacion, @primeraComunion, CURRENT_TIMESTAMP);

	SELECT TOP 1 idAlumno from Alumno ORDER BY fechaRegistrado DESC;

GO
USE [master]
GO
ALTER DATABASE [colegio] SET  READ_WRITE 
GO
